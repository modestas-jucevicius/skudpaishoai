# import gc
import errno
import os
import random

# from pympler import summary, muppy

from game import boardManager, moveManager, notationObject
from memory import Memory
import pickle

from model import newModel as model
from model.newModel import NeuralNetwork
import torch
from shutil import copyfile
from MCTS.agent import Agent as MCTSAgent
from ABPruning.agent import Agent as ABAgent
# from pympler.tracker import SummaryTracker
# gc.set_debug(gc.DEBUG_UNCOLLECTABLE)
# tracker = SummaryTracker()
# sum1 = summary.summarize(muppy.get_objects())
# from mem_top import mem_top
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model.device = device

INITIAL_MEMORY_VERSION = None
INITIAL_RUN_NUMBER = 37
INITIAL_MODEL_VERSION = 2
INITIAL_TRAIN = False
START_STATE = None
MEMORY_TXT = False
TOURNAMENT_FIRST = True
ANOTHER_MODEL_VERSION = None

def playMatches(player1, player2, EPISODES, turns_until_tau0, memory=None, goes_first=0):
    scores = {player1.name: 0, "drawn": 0, player2.name: 0}
    sp_scores = {'sp': 0, "drawn": 0, 'nsp': 0}
    points = {player1.name: [], player2.name: []}

    for e in range(EPISODES):
        print(str(e + 1) + ' ', end='')
        state = boardManager.Board("H")
        if START_STATE is not None:
            moves = START_STATE.split(';')[:-1]
            notationMoves = []
            for move in moves:
                notationMoves.append(notationObject.NotationMove(move))
            for notationMove in notationMoves:
                moveManager.runNotationMove(notationMove, state)
        done = 0
        turn = 0
        player1.searchPolicy = None
        player2.searchPolicy = None
        gameNotation = ""

        if goes_first == 0:
            player1Starts = random.randint(0, 1) * 2 - 1
        else:
            player1Starts = goes_first

        if player1Starts == 1:
            players = {1: {"agent": player1, "name": player1.name}
                , -1: {"agent": player2, "name": player2.name}
                       }
        else:
            players = {1: {"agent": player2, "name": player2.name}
                , -1: {"agent": player1, "name": player1.name}
                       }

        while done == 0:
            turn = turn + 1

            #### Run the MCTS algo and return an action
            playerTurn = 1
            if state.playerCode == "G":
                playerTurn = -1

            if turn < turns_until_tau0:
                action, pi, MCTS_value, NN_value = players[playerTurn]['agent'].act(state, 1)
            else:
                action, pi, MCTS_value, NN_value = players[playerTurn]['agent'].act(state, 0)

            # possibleMoves = state.moveManager.getPossibleMoves(state, state.playerCode)
            # print(len(possibleMoves))

            # if turn % 10 == 0:
                # print(str(turn))
                # print(gameNotation)

            # if turn % 10 == 0:
            #     print(mem_top())  # Or just print().

            ### Do the action
            nextState = state.getCopy()
            moveManager.runNotationMove(action, nextState)
            gameNotation += action.fullMoveText + ';'

            value = 0
            if config.MODIFY_WIN_CONDITIONS:
                if config.CROSS_MIDLINES:
                    if nextState.harmonyManager.chainQuadrantNumberForPlayer("H") >= config.HARMONIES_TO_WIN:
                        nextState.winners.append("H")
                    if nextState.harmonyManager.chainQuadrantNumberForPlayer("G") >= config.HARMONIES_TO_WIN:
                        nextState.winners.append("G")
                else:
                    if nextState.harmonyManager.numHarmoniesForPlayer("H") >= config.HARMONIES_TO_WIN:
                        nextState.winners.append("H")
                    if nextState.harmonyManager.numHarmoniesForPlayer("G") >= config.HARMONIES_TO_WIN:
                        nextState.winners.append("G")

            if memory != None:
                # tracker = SummaryTracker()
                ####Commit the move to memory
                memory.commit_stmemory(nextState, pi, MCTS_value)
            if len(nextState.winners) > 0:
                done = 1
                if "G" in nextState.winners and len(nextState.winners) == 1:
                    value = -1
                if "H" in nextState.winners and len(nextState.winners) == 1:
                    value = 1

            state = nextState

            if turn % 200 == 0:
                e -= 1
                break

            # sum2 = summary.summarize(muppy.get_objects())
            # tracker.print_diff(summary.get_diff(sum1, sum2))

            if done == 1:
                print(gameNotation)
                if memory != None:
                    memory.commit_ltmemory()

                if value == 1:
                    playerTurn = 1
                    scores[players[playerTurn]['name']] = scores[players[playerTurn]['name']] + 1
                    sp_scores['sp'] = sp_scores['sp'] + 1

                elif value == -1:
                    playerTurn = -1
                    scores[players[playerTurn]['name']] = scores[players[playerTurn]['name']] + 1
                    sp_scores['nsp'] = sp_scores['nsp'] + 1
                else:
                    scores['drawn'] = scores['drawn'] + 1
                    sp_scores['drawn'] = sp_scores['drawn'] + 1

    if memory is not None:
        memory.clear_stmemory()

        if len(memory.ltmemory) >= config.LONG_MEMORY_SIZE:
            # ####### RETRAINING ########
            print('RETRAINING...')
            fileName = "./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + "/memory/memory" + str(iteration).zfill(
                4) + ".p"
            if not os.path.exists(os.path.dirname(fileName)):
                try:
                    os.makedirs(os.path.dirname(fileName))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise
            pickle.dump(memory, open(fileName, "wb"))
            fileName = "./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + "/memory/plot" + str(iteration).zfill(
                4) + ".png"
            current_player.replay(memory.ltmemory, fileName)
            print('')

    return scores, memory, sp_scores


if __name__ == '__main__':
    if INITIAL_RUN_NUMBER != None:
        copyfile("./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + '/config.py', './config.py')

    import config

    if INITIAL_MEMORY_VERSION is None:
        memory = Memory()
        if MEMORY_TXT:
            with open("memory3.txt") as f:
                content = f.read().splitlines()
            for line in content:
                line = line.split(" ")[1]
                moves = line.split(';')[:-1]
                notationMoves = []
                state = boardManager.Board("H")
                for move in moves:
                    notationMoves.append(notationObject.NotationMove(move))
                for notationMove in notationMoves:
                    moveManager.runNotationMove(notationMove, state)
                value = 0

                if config.MODIFY_WIN_CONDITIONS:
                    if config.CROSS_MIDLINES:
                        if state.harmonyManager.chainQuadrantNumberForPlayer("H") >= config.HARMONIES_TO_WIN:
                            state.winners.append("H")
                        if state.harmonyManager.chainQuadrantNumberForPlayer("G") >= config.HARMONIES_TO_WIN:
                            state.winners.append("G")
                    else:
                        if state.harmonyManager.numHarmoniesForPlayer("H") >= config.HARMONIES_TO_WIN:
                            state.winners.append("H")
                        if state.harmonyManager.numHarmoniesForPlayer("G") >= config.HARMONIES_TO_WIN:
                            state.winners.append("G")

                    if len(state.winners) > 0:
                        done = 1
                        if "G" in state.winners and len(state.winners) == 1:
                            value = -1
                        if "H" in state.winners and len(state.winners) == 1:
                            value = 1

                    memory.commit_stmemory(state, None, value)
                    memory.commit_ltmemory()
    else:
        print('LOADING MEMORY VERSION ' + str(INITIAL_MEMORY_VERSION) + '...')
        memory = pickle.load( open("./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + "/memory/memory" + str(INITIAL_MEMORY_VERSION).zfill(4) + ".p",   "rb"))

    current_NN = NeuralNetwork().to(device)
    best_NN = NeuralNetwork().to(device)

    # ####### LOAD MODEL IF NECESSARY ########

    if INITIAL_MODEL_VERSION is not None:
        best_player_version = INITIAL_MODEL_VERSION
        print('LOADING MODEL VERSION ' + str(INITIAL_MODEL_VERSION) + '...')
        best_NN.load_state_dict(torch.load("./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + "/models/version" + "{0:0>4}".format(best_player_version) + "/model_weights.pth"))
        current_NN.load_state_dict(torch.load("./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + "/models/version" + "{0:0>4}".format(best_player_version) + "/model_weights.pth"))
        if ANOTHER_MODEL_VERSION is not None:
            current_NN.load_state_dict(torch.load(
                "./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + "/models/version" + "{0:0>4}".format(
                    ANOTHER_MODEL_VERSION) + "/model_weights.pth"))
    # otherwise just ensure the weights on the two players are the same
    else:
        best_player_version = 0
        best_NN.load_state_dict(current_NN.state_dict())

    current_player = MCTSAgent("current_player", config.CPUCT, current_NN)
    best_player = MCTSAgent("best_player", config.CPUCT, best_NN)
    iteration = 0

    if INITIAL_TRAIN:
        fileName = "./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + "/memory/plot" + str(iteration).zfill(
            4) + ".png"
        current_player.replay(memory.ltmemory, fileName)



    while 1:
        iteration += 1
        print('ITERATION NUMBER ' + str(iteration))
        print('BEST PLAYER VERSION ' + str(best_player_version))

        ##### ### SELF PLAY ########
        # print('SELF PLAYING ' + str(config.EPISODES) + ' EPISODES...')
        # if not TOURNAMENT_FIRST:
        #     _, memory, _ = playMatches(best_player, best_player, config.EPISODES, turns_until_tau0=config.TURNS_UNTIL_TAU0, memory=memory)

        # ####### TOURNAMENT ########
        print('TOURNAMENT...')
        current_player_tournament = ABAgent("current_player", config.CPUCT, current_player.model, 5)
        best_player_tournament = MCTSAgent("best_player", config.CPUCT, best_player.model)
        scores, _, sp_scores = playMatches(best_player_tournament, current_player_tournament, config.EVAL_EPISODES, turns_until_tau0=0, goes_first=0)
        print('\nSCORES')
        print(scores)
        print('\nSTARTING PLAYER / NON-STARTING PLAYER SCORES')
        print(sp_scores)

        print('\n\n')

        if scores['current_player'] > scores['best_player'] * config.SCORING_THRESHOLD:
            best_player_version = best_player_version + 1
            best_NN.load_state_dict(current_NN.state_dict())
            fileName = "./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + "/models/version" + "{0:0>4}".format(best_player_version) + '/model_weights.pth'
            if not os.path.exists(os.path.dirname(fileName)):
                try:
                    os.makedirs(os.path.dirname(fileName))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise
            torch.save(best_NN.state_dict(), fileName)
            fileName = "./run_archive/" + str(INITIAL_RUN_NUMBER).zfill(4) + "/config.py"
            if not os.path.exists(os.path.dirname(fileName)):
                try:
                    os.makedirs(os.path.dirname(fileName))
                except OSError as exc:  # Guard against race condition
                    if exc.errno != errno.EEXIST:
                        raise
            copyfile('./config.py', fileName)

        TOURNAMENT_FIRST = False
