# %matplotlib inline
import collections
import itertools

import numpy as np
import random

import torch
from joblib import Parallel, delayed

import config

from ABPruning.ABPrunning import ABPrunning, Node, Edge
from game import boardManager, moveManager, gameStateManager
#from game.boardManager import Board
#from loss import softmax_cross_entropy_with_logits

#import config
#import loggers as lg
import time

import matplotlib.pyplot as plt
from IPython import display
import pylab as pl
from model import newModel as model
# from pympler.tracker import SummaryTracker


class Agent:
    def __init__(self, name, cpuct, model, prunningWidth):
        self.name = name

        self.cpuct = cpuct

        self.model = model

        self.searchPolicy: ABPrunning = None
        self.root = None

        self.train_overall_loss = []
        self.test_overall_loss = []
        self.train_value_loss = []
        self.train_policy_loss = []
        self.val_overall_loss = []
        self.val_value_loss = []
        self.val_policy_loss = []
        self.prunningWidth = prunningWidth


    def act(self, state, tau):
        # tracker = SummaryTracker()
        if self.searchPolicy is None or state.id not in self.searchPolicy.tree:
            self.buildABPrunning(state)
        else:
            self.changeRootABPrunning(state)

        # run the simulation
        startTime = time.time()
        alpha = -999
        beta = 999
        self.searchPolicy.depthLimit = 1
        self.searchPolicy.startTime = startTime
        self.searchPolicy.count = 0
        while time.time() - startTime < config.TURN_TIME_AB:
            if state.playerCode == 'H':
                self.searchPolicy.maxValue(self.searchPolicy.root, alpha, beta, 0)
            else:
                self.searchPolicy.minValue(self.searchPolicy.root, alpha, beta, 0)

            self.searchPolicy.depthLimit += 1

        # print(self.searchPolicy.depthLimit - 1)
        # get action values
        # if state.playerCode == 'H':
        #     self.searchPolicy.maxValue(self.searchPolicy.root, alpha, beta, 0)
        # else:
        #     self.searchPolicy.minValue(self.searchPolicy.root, alpha, beta, 0)
        pi, values = self.getAV(1, state)

        # pick the action
        action, value = self.chooseAction(pi, values, tau, state)

        nextState = state.getCopy()
        moveManager.runNotationMove(self.searchPolicy.root.edges[action].action, nextState)

        NN_value = self.get_preds(nextState)
        self.searchPolicy.breadcrumbs.append(state)

        # if turn % 10 == 0:
        # tracker.print_diff()

        return self.searchPolicy.root.edges[action].action, pi, value, NN_value

    def get_preds(self, state: boardManager.Board):
        # predict the leaf

        inputToModel = torch.from_numpy(np.array([gameStateManager.getGameState(state).flatten()])).float().to(model.device)

        preds = self.model(inputToModel)
        prob_array = preds
        value = torch.argmax(prob_array[0]) - 1

        return value

    def getAV(self, tau, state):
        edges = self.searchPolicy.root.edges
        pi = np.zeros(len(edges), dtype=np.float64)
        values = np.zeros(len(edges), dtype=np.int32)

        for index, edge in enumerate(edges):
            if state.playerCode == "H":
                pi[index] = pow(edge.stats['P'], 1 / tau)
            else:
                pi[index] = pow(edge.stats['O'], 1 / tau)

            N = 1 - edge.stats['P'] - edge.stats['O']

            values[index] = torch.argmax(torch.as_tensor([edge.stats['O'], N, edge.stats['P']])) - 1

            pi = pi.astype('float64')
            if np.sum(pi) != 0:
                pi = pi / (np.sum(pi) * 1.0)

        return pi, values

    def chooseAction(self, pi, values, tau, state):
        if tau == 0:
            actions = np.argwhere(pi == max(pi))
            action = random.choice(actions)[0]
        else:
            action_idx = np.random.multinomial(1, pi)
            action = np.where(action_idx == 1)[0][0]

        value = values[action]

        return action, value

    def buildABPrunning(self, state):
        self.root = Node(state)
        self.searchPolicy = ABPrunning(self.root, self.cpuct, self.model, self.prunningWidth)

    def changeRootABPrunning(self, state):
        self.searchPolicy.root = self.searchPolicy.tree[state.id]
        self.searchPolicy.tree = {}
        subTreeNodes = [self.searchPolicy.root]
        while len(subTreeNodes) != 0:
            subTreeNode = subTreeNodes.pop(0)
            for edge in subTreeNode.edges:
                if edge.outNode is not None:
                    subTreeNodes.append(edge.outNode)

            self.searchPolicy.addNode(subTreeNode)


    def replay(self, ltmemory, fileName):
        training_memory = collections.deque(itertools.islice(ltmemory, int(len(ltmemory)*0.9)))
        test_memory = collections.deque(itertools.islice(ltmemory, len(training_memory), len(ltmemory)))

        for i in range(config.TRAINING_LOOPS):
            minibatch = random.sample(training_memory, min(config.BATCH_SIZE, len(training_memory)))

            training_states = np.array([gameStateManager.getGameState(row['board']).flatten() for row in minibatch])
            training_targets = np.array([row['value'] for row in minibatch])

            testminibatch = random.sample(test_memory, min(config.BATCH_SIZE, len(test_memory)))
            test_states = np.array([gameStateManager.getGameState(row['board']).flatten() for row in testminibatch])
            test_targets = np.array([row['value'] for row in testminibatch])

            loss = None
            lossTest = None
            for t in range(config.EPOCHS):
                loss = model.train(training_states, training_targets, self.model,
                                                            self.model.loss_fn, self.model.optimizer)
                lossTest = model.test(test_states, test_targets, self.model)


            self.train_overall_loss.append(round(loss, 4))
            self.test_overall_loss.append(round(lossTest, 4))

        plt.plot(self.train_overall_loss, 'k')
        plt.plot(self.test_overall_loss, 'r')

        testminibatch = random.sample(test_memory, min(config.BATCH_SIZE, len(test_memory)))
        test_states = np.array([gameStateManager.getGameState(row['board']).flatten() for row in testminibatch])
        test_targets = np.array([row['value'] for row in testminibatch])
        model.test(test_states, test_targets, self.model, printResult=True)

        plt.legend(['train_overall_loss', 'test_overall_loss'], loc='lower left')
        plt.savefig(fileName)
        display.clear_output(wait=True)
        display.display(pl.gcf())
        pl.gcf().clear()
        time.sleep(1.0)


def my_floor(a, precision=0):
    return np.true_divide(np.floor(a * 10**precision), 10**precision)

