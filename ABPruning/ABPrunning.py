import time

import numpy as np
import torch

import config
from game import boardManager, moveManager, gameStateManager
from model import newModel as model

EPSILON = 0.2
ALPHA = 0.8


class Node:
	def __init__(self, state: boardManager.Board):
		self.state = state
		self.playerTurn = state.playerCode
		self.id = state.id
		self.edges = []
		self.unpacked = False
		self.numberOfPossibleMoves = 0

	def isLeaf(self):
		if len(self.edges) > 0:
			return False
		else:
			return True


class Edge:
	def __init__(self, inNode, outNode, action):
		outNodeId = ""
		if outNode is not None:
			outNodeId = outNode.state.id
		self.id = inNode.state.id + '|' + outNodeId
		self.inNode = inNode
		self.outNode = outNode
		self.playerTurn = inNode.state.playerCode
		self.action = action

		self.stats = {
					'N': 0,
					'W': 0,
					'Q': 0,
					'P': 0,
					'O': 0
				}
				

class ABPrunning:
	def __init__(self, root: Node, cpuct, model, prunningWidth):
		self.root = root
		self.tree = {}
		self.cpuct = cpuct
		self.addNode(root)
		self.model = model
		self.depthLimit = 1
		self.startTime = 0
		self.breadcrumbs = []
		self.count = 0
		self.prunningWidth = prunningWidth
	
	def __len__(self):
		return len(self.tree)

	def maxValue(self, node: Node, alpha, beta, depth):
		if depth == 2:
			self.count +=1
		maxScore = -999
		maxP = 0
		maxO = 0
		state = node.state
		if config.MODIFY_WIN_CONDITIONS:
			if config.CROSS_MIDLINES:
				state.winners = []
				if state.harmonyManager.chainQuadrantNumberForPlayer("H") >= config.HARMONIES_TO_WIN:
					state.winners.append("H")
				if state.harmonyManager.chainQuadrantNumberForPlayer("G") >= config.HARMONIES_TO_WIN:
					state.winners.append("G")
			else:
				if state.harmonyManager.numHarmoniesForPlayer("H") >= config.HARMONIES_TO_WIN:
					state.winners.append("H")
				if state.harmonyManager.numHarmoniesForPlayer("G") >= config.HARMONIES_TO_WIN:
					state.winners.append("G")

		if len(state.winners) > 0:
			if len(state.winners) == 2:
				return 0, 0

			if self.root.state.playerCode in state.winners:
				return 0, 1

			return 1, 0

		if depth == self.depthLimit:
			_, probs = self.get_preds(state)
			return probs[0], probs[2]

		node.W = 0
		if node.state.playerCode == "H":
			reverseSort = True
		else:
			reverseSort = False
		if len(node.edges) == 0 or not node.unpacked:
			allowedActions = state.moveManager.getPossibleMoves(state, state.playerCode)
			# print(len(allowedActions))
			for allowedAction in allowedActions:
				if time.time() - self.startTime > config.TURN_TIME_AB:
					if maxScore > -999:
						return maxO, maxP
					else:
						_, probs = self.get_preds(state)
						return probs[0], probs[2]

				newState = state.getCopy()
				moveManager.runNotationMove(allowedAction, newState)
				if newState.id in self.tree:
					continue

				newNode = Node(newState)
				self.addNode(newNode)
				newEdge = Edge(node, newNode, allowedAction)
				node.edges.append(newEdge)
				O, P = self.minValue(newNode, alpha, beta, depth + 1)
				newEdge.stats['P'] = P
				newEdge.stats['O'] = O

				if P - O > maxScore:
					maxScore = P - O
					maxO = O
					maxP = P
					alpha = max(alpha, maxScore)
				if alpha > beta:
					node.edges.sort(key=self.getP, reverse=reverseSort)
					if self.prunningWidth != 0 and len(node.edges) > self.prunningWidth:
						node.edges = node.edges[:self.prunningWidth - 1]
						node.unpacked = True
					return maxO, maxP
		else:
			for edge in node.edges:
				if time.time() - self.startTime > config.TURN_TIME_AB:
					if maxScore > -999:
						return maxO, maxP
					else:
						_, probs = self.get_preds(state)
						return probs[0], probs[2]
				O, P = self.minValue(edge.outNode, alpha, beta, depth + 1)
				edge.stats['P'] = P
				edge.stats['O'] = O

				if P - O > maxScore:
					maxScore = P - O
					maxO = O
					maxP = P
					alpha = max(alpha, maxScore)
				if alpha > beta:
					node.edges.sort(key=self.getP, reverse=reverseSort)
					if self.prunningWidth != 0 and len(node.edges) > self.prunningWidth:
						node.edges = node.edges[:self.prunningWidth - 1]
					return maxO, maxP

		node.unpacked = True
		node.edges.sort(key=self.getP, reverse=reverseSort)
		if self.prunningWidth != 0 and len(node.edges) > self.prunningWidth:
			node.edges = node.edges[:self.prunningWidth - 1]
		return maxO, maxP

	def minValue(self, node: Node, alpha, beta, depth):
		if depth == 2:
			self.count +=1
		minScore = 999
		maxP = 0
		maxO = 0
		state = node.state
		if config.MODIFY_WIN_CONDITIONS:
			state.winners = []
			if config.CROSS_MIDLINES:
				if state.harmonyManager.chainQuadrantNumberForPlayer("H") >= config.HARMONIES_TO_WIN:
					state.winners.append("H")
				if state.harmonyManager.chainQuadrantNumberForPlayer("G") >= config.HARMONIES_TO_WIN:
					state.winners.append("G")
			else:
				if state.harmonyManager.numHarmoniesForPlayer("H") >= config.HARMONIES_TO_WIN:
					state.winners.append("H")
				if state.harmonyManager.numHarmoniesForPlayer("G") >= config.HARMONIES_TO_WIN:
					state.winners.append("G")
		if len(state.winners) > 0:
			if len(state.winners) == 2:
				return 0, 0

			if self.root.state.playerCode in state.winners:
				return 0, 1

			return 1, 0

		if depth == self.depthLimit:
			_, probs = self.get_preds(state)
			return probs[0], probs[2]

		node.W = 0
		if node.state.playerCode == "H":
			reverseSort = True
		else:
			reverseSort = False
		if len(node.edges) == 0 or not node.unpacked:
			allowedActions = state.moveManager.getPossibleMoves(state, state.playerCode)
			# print(len(allowedActions))
			node.numberOfPossibleMoves = len(allowedActions)
			for allowedAction in allowedActions:
				if time.time() - self.startTime > config.TURN_TIME_AB:
					if minScore < 999:
						return maxO, maxP
					else:
						_, probs = self.get_preds(state)
						return probs[0], probs[2]
				newState = state.getCopy()
				moveManager.runNotationMove(allowedAction, newState)
				if newState.id in self.tree:
					continue

				newNode = Node(newState)
				self.addNode(newNode)
				newEdge = Edge(node, newNode, allowedAction)
				node.edges.append(newEdge)
				O, P = self.maxValue(newNode, alpha, beta, depth + 1)
				newEdge.stats['P'] = P
				newEdge.stats['O'] = O

				if P - O < minScore:
					minScore = P - O
					maxO = O
					maxP = P
					beta = min(beta, minScore)

				if minScore < alpha:
					node.edges.sort(key=self.getP, reverse=reverseSort)
					if self.prunningWidth != 0 and len(node.edges) > self.prunningWidth:
						node.edges = node.edges[:self.prunningWidth - 1]
						node.unpacked = True
					return maxO, maxP
		else:
			for edge in node.edges:
				if time.time() - self.startTime > config.TURN_TIME_AB:
					if minScore < 999:
						return maxO, maxP
					else:
						_, probs = self.get_preds(state)
						return probs[0], probs[2]

				O, P = self.maxValue(edge.outNode, alpha, beta, depth + 1)
				edge.stats['P'] = P
				edge.stats['O'] = O

				if P - O < minScore:
					minScore = P - O
					maxO = O
					maxP = P
					beta = min(beta, minScore)

				if minScore < alpha:
					node.edges.sort(key=self.getP, reverse=reverseSort)
					if self.prunningWidth != 0 and len(node.edges) > self.prunningWidth:
						node.edges = node.edges[:self.prunningWidth - 1]
					return maxO, maxP

		node.unpacked = True
		node.edges.sort(key=self.getP, reverse=reverseSort)
		if self.prunningWidth != 0 and len(node.edges) > self.prunningWidth:
			node.edges = node.edges[:self.prunningWidth - 1]
		return maxO, maxP

	def getP(self, edge):
		return edge.stats['P'] - edge.stats['O']


	def get_preds(self, state: boardManager.Board):
		# predict the leaf
		inputToModel = torch.from_numpy(np.array([gameStateManager.getGameState(state).flatten()])).float().to(model.device)
		preds = self.model(inputToModel)
		prob_array = preds
		value = torch.argmax(prob_array[0]) - 1
		return value.item(), preds.flatten()

	def addNode(self, node):
		self.tree[node.id] = node
