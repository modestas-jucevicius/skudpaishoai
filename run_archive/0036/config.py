EPISODES = 21
MCTS_SIMS = 5
LONG_MEMORY_SIZE = 20
SHORT_MEMORY_SIZE = 1
TURNS_UNTIL_TAU0 = 1000 # turn on which it starts playing deterministically
CPUCT = 1
EPSILON = 0.2
ALPHA = 0.8

# ### RETRAINING
BATCH_SIZE = 500
EPOCHS = 800
REG_CONST = 0.0001
LEARNING_RATE = 1e-4
MOMENTUM = 0.9
TRAINING_LOOPS = 100

# ### EVALUATION
EVAL_EPISODES = 1
SCORING_THRESHOLD = 1.3

# GAME
SIMPLE_FLOWER_TILE_NUMBER = 2

TURN_TIME = 30
TURN_TIME_AB = 30

HIDDEN_LAYERS = 2
LAYER_SIZE = 200

MODIFY_WIN_CONDITIONS = True
HARMONIES_TO_WIN = 3
CROSS_MIDLINES = True

AB_TREE_PRUNE = 0