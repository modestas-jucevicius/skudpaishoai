from collections import deque

import config

class Memory:
	def __init__(self):
		self.ltmemory = deque(maxlen=config.LONG_MEMORY_SIZE)
		self.stmemory = deque(maxlen=config.SHORT_MEMORY_SIZE)

	def commit_stmemory(self, state, actionValues, value):
		self.stmemory.append({
			'board': state,
			'AV': actionValues,
			'value': value
			})

	def commit_ltmemory(self):
		for i in self.stmemory:
			self.ltmemory.append(i)
		self.clear_stmemory()

	def clear_stmemory(self):
		self.stmemory = deque(maxlen=config.SHORT_MEMORY_SIZE)
