import random

import numpy as np
import torch

import config
from game import boardManager, moveManager, gameStateManager

from model import newModel as model

EPSILON = 0.2
ALPHA = 0.8


class Node:
	def __init__(self, state: boardManager.Board):
		self.state = state
		self.playerTurn = state.playerCode
		self.id = state.id
		self.edges = []
		self.possibleMoves = []

	def isLeaf(self):
		if len(self.edges) > 0:
			return False
		else:
			return True


class Edge:
	def __init__(self, inNode, outNode, action):
		outNodeId = ""
		if outNode is not None:
			outNodeId = outNode.state.id
		self.id = inNode.state.id + '|' + outNodeId
		self.inNode = inNode
		self.outNode = outNode
		self.playerTurn = inNode.state.playerCode
		self.action = action

		self.stats = {
					'N': 0,
					'W': 0,
					'Q': 0,
					'P': 0,
					'L': 0,
					'D': 0,
					'LQ': 0,
					'DQ': 0,
					'WQ': 0,
				}


class MCTS:
	def __init__(self, root: Node, cpuct, model):
		self.root = root
		self.tree = {}
		self.cpuct = cpuct
		self.addNode(root)
		self.model = model

	def __len__(self):
		return len(self.tree)

	def moveToLeaf(self):
		breadcrumbs = []
		currentNode = self.root

		done = 0
		value = 0
		if len(currentNode.possibleMoves) == 0:
			currentNode.possibleMoves = currentNode.state.moveManager.getPossibleMoves(currentNode.state,
																					   currentNode.state.playerCode)

		while not currentNode.isLeaf():
			if len(currentNode.possibleMoves) == 0:
				currentNode.possibleMoves = currentNode.state.moveManager.getPossibleMoves(currentNode.state, currentNode.state.playerCode)

			moveIndex = random.randint(0, len(currentNode.possibleMoves) - 1)
			move = currentNode.possibleMoves[moveIndex]
			newState = currentNode.state.getCopy()
			moveManager.runNotationMove(move, newState)
			if newState.id not in self.tree:
				node = Node(newState)
				self.addNode(node)
				newEdge = Edge(currentNode, node, move)
				_, P = self.get_preds(newState)
				newEdge.stats['P'] = P
				currentNode.edges.append(newEdge)

			simulationEdge = self.getSimulationEdge(currentNode, breadcrumbs)

			if config.MODIFY_WIN_CONDITIONS:
				simulationEdge.outNode.state.winners = []
				if config.CROSS_MIDLINES:
					if simulationEdge.outNode.state.harmonyManager.chainQuadrantNumberForPlayer("H") >= config.HARMONIES_TO_WIN:
						simulationEdge.outNode.state.winners.append("H")
					if simulationEdge.outNode.state.harmonyManager.chainQuadrantNumberForPlayer("G") >= config.HARMONIES_TO_WIN:
						simulationEdge.outNode.state.winners.append("G")
				else:
					if simulationEdge.outNode.state.harmonyManager.numHarmoniesForPlayer("H") >= config.HARMONIES_TO_WIN:
						simulationEdge.outNode.state.winners.append("H")
					if simulationEdge.outNode.state.harmonyManager.numHarmoniesForPlayer("G") >= config.HARMONIES_TO_WIN:
						simulationEdge.outNode.state.winners.append("G")

			if len(simulationEdge.outNode.state.winners) > 0:
				done = 1

			# value negative because it's in perspective of next player, the previous won, therefore you lost
			if "G" in simulationEdge.outNode.state.winners and len(simulationEdge.outNode.state.winners) == 1:
				value = -1

			if "H" in simulationEdge.outNode.state.winners and len(simulationEdge.outNode.state.winners) == 1:
				value = 1

			# simulationEdge.stats['N'] += 1
			currentNode = simulationEdge.outNode
			breadcrumbs.append((simulationEdge, simulationEdge.inNode))

		if len(currentNode.possibleMoves) == 0:
			currentNode.possibleMoves = currentNode.state.moveManager.getPossibleMoves(currentNode.state, currentNode.state.playerCode)

		if done == 0 and len(currentNode.possibleMoves) > 0:
			moveIndex = random.randint(0, len(currentNode.possibleMoves) - 1)
			move = currentNode.possibleMoves[moveIndex]
			newState = currentNode.state.getCopy()
			moveManager.runNotationMove(move, newState)
			if newState.id not in self.tree:
				node = Node(newState)
				self.addNode(node)
				newEdge = Edge(currentNode, node, move)
				_, P = self.get_preds(newState)
				newEdge.stats['P'] = P
				currentNode.edges.append(newEdge)

		return currentNode, value, done, breadcrumbs

	def getSimulationEdge(self, currentNode, breadcrumbs):
		maxQU = -99999
		Nb = 0

		if currentNode == self.root:
			epsilon = config.EPSILON
			nu = np.random.dirichlet([config.ALPHA] * len(currentNode.edges))
		else:
			epsilon = 0
			nu = [0] * len(currentNode.edges)

		for edge in currentNode.edges:
			Nb = Nb + edge.stats['N']

		possibleEdges = []
		for idx, edge in enumerate(currentNode.edges):
			# loop = False
			# for (old_edge, old_node) in breadcrumbs:
			# 	if edge.outNode == old_node:
			# 		loop = True
			# 		break
			#
			# if loop:
			# 	continue
			U = self.cpuct * \
				((1 - epsilon) * edge.stats['P'] + epsilon * nu[idx]) * \
				np.sqrt(Nb) / (1 + edge.stats['N'])
			# U = (1 + abs(edge.stats['P'])) * self.cpuct
			# if Nb != 0:
			# 	U = (edge.stats['N'] / Nb + edge.stats['P'] / Nb) * self.cpuct

			Q = edge.stats['Q']

			if Q + U > maxQU:
				maxQU = Q + U
				possibleEdges = [edge]
			elif Q + U == maxQU:
				possibleEdges.append(edge)

		edgeIndex = random.randint(0, len(possibleEdges) - 1)
		simulationEdge = possibleEdges[edgeIndex]

		return simulationEdge

	# def backFill(self, leaf, value, breadcrumbs):
	# 	for (edge, node) in breadcrumbs:
	# 		edge.stats['W'] = edge.stats['W'] + value
	# 		edge.stats['Q'] = edge.stats['W'] / edge.stats['N']

	def backFill(self, leaf, value, breadcrumbs):
		for (edge, node) in breadcrumbs:
			edge.stats['N'] = edge.stats['N'] + 1
			if value == 1:
				edge.stats['W'] += 10
			elif value == -1:
				edge.stats['L'] += 1
			else:
				edge.stats['D'] += 1

			edge.stats['WQ'] = edge.stats['W'] / edge.stats['N']
			edge.stats['DQ'] = edge.stats['D'] / edge.stats['N']
			edge.stats['LQ'] = edge.stats['L'] / edge.stats['N']

			if edge.outNode.state.playerCode == "G":
				edge.stats['Q'] = edge.stats['L'] / edge.stats['N']
			else:
				edge.stats['Q'] = edge.stats['W'] / edge.stats['N']


	def addNode(self, node):
		self.tree[node.id] = node

	def get_preds(self, state: boardManager.Board):
		# predict the leaf

		inputToModel = torch.from_numpy(np.array([gameStateManager.getGameState(state).flatten()])).float().to(
			model.device)

		preds = self.model(inputToModel)
		prob_array = preds
		value = torch.argmax(prob_array[0]) - 1
		if state.playerCode == "G":
			return value, preds[0][2]

		return value, preds[0][0]

