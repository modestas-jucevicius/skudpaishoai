# %matplotlib inline
import collections
import itertools

import numpy as np
import random

import torch
from joblib import Parallel, delayed

import config

from MCTS.MCTS import MCTS, Node, Edge
from game import boardManager, moveManager, gameStateManager
#from game.boardManager import Board
#from loss import softmax_cross_entropy_with_logits

#import config
#import loggers as lg
import time

import matplotlib.pyplot as plt
from IPython import display
import pylab as pl
from model import newModel as model
# from pympler.tracker import SummaryTracker


class Agent:
    def __init__(self, name, cpuct, model):
        self.name = name

        self.cpuct = cpuct

        self.model = model

        self.searchPolicy = None
        self.root = None
        self.highestDepth = 0

        self.train_overall_loss = []
        self.test_overall_loss = []
        self.train_value_loss = []
        self.train_policy_loss = []
        self.val_overall_loss = []
        self.val_value_loss = []
        self.val_policy_loss = []

    def simulate(self):
        # MOVE THE LEAF NODE (SELECTION)
        leaf, value, done, breadcrumbs = self.searchPolicy.moveToLeaf()

        if len(breadcrumbs) > self.highestDepth:
            self.highestDepth = len(breadcrumbs)

        # EVALUATE THE LEAF NODE (
        value, breadcrumbs = self.evaluateLeaf(leaf, value, done, breadcrumbs)

        # BACKFILL THE VALUE THROUGH THE TREE
        self.searchPolicy.backFill(leaf, value, breadcrumbs)

    def act(self, state, tau):
        # tracker = SummaryTracker()
        if self.searchPolicy is None or state.id not in self.searchPolicy.tree:
            self.buildMCTS(state)
        else:
            self.changeRootMCTS(state)

        # run the simulation
        startTime = time.time()
        while (time.time() - startTime < config.TURN_TIME):
            self.simulate()

        print(self.highestDepth)
        print(len(self.searchPolicy.root.edges))
        self.highestDepth = 0
        # get action values
        pi, values = self.getAV(1)

        # pick the action
        action, value = self.chooseAction(pi, values, tau)

        nextState = state.getCopy()
        moveManager.runNotationMove(self.searchPolicy.root.edges[action].action, nextState)

        NN_value = self.get_preds(nextState)

        # if turn % 10 == 0:
        # tracker.print_diff()

        return self.searchPolicy.root.edges[action].action, pi, value, NN_value

    def get_preds(self, state: boardManager.Board):
        # predict the leaf

        inputToModel = torch.from_numpy(np.array([gameStateManager.getGameState(state).flatten()])).float().to(
            model.device)

        preds = self.model(inputToModel)
        prob_array = preds
        value = torch.argmax(prob_array[0]) - 1

        return value

    def evaluateLeaf(self, leaf, value, done, breadcrumbs):
        if done == 0:
            value = self.get_preds(leaf.state)

        return value, breadcrumbs

    def getAV(self, tau):
        edges = self.searchPolicy.root.edges
        pi = np.zeros(len(edges), dtype=np.integer)
        values = np.zeros(len(edges), dtype=np.float64)

        for index, edge in enumerate(edges):
            pi[index] = pow(edge.stats['N'], 1 / tau)

            values[index] = torch.argmax(torch.as_tensor([edge.stats['LQ'], edge.stats['DQ'], edge.stats['WQ']])) - 1
            if edge.stats['LQ'] == edge.stats['WQ'] or edge.stats['N'] == 0:
                values[index] = 0

        if np.sum(pi) != 0:
            pi = pi / (np.sum(pi) * 1.0)

        return pi, values

    def chooseAction(self, pi, values, tau):
        if tau == 0:
            actions = np.argwhere(pi == max(pi))
            action = random.choice(actions)[0]
        else:
            action_idx = np.random.multinomial(1, pi)
            action = np.where(action_idx == 1)[0][0]

        value = values[action]

        return action, value

    def buildMCTS(self, state):
        self.searchPolicy = MCTS(Node(state), self.cpuct, self.model)

    def changeRootMCTS(self, state):
        self.searchPolicy.root = self.searchPolicy.tree[state.id]
        self.searchPolicy.tree = {}
        subTreeNodes = [self.searchPolicy.root]
        while len(subTreeNodes) != 0:
            subTreeNode = subTreeNodes.pop(0)
            for edge in subTreeNode.edges:
                if edge.outNode is not None:
                    subTreeNodes.append(edge.outNode)

            self.searchPolicy.addNode(subTreeNode)

    def replay(self, ltmemory, fileName):
        training_memory = collections.deque(itertools.islice(ltmemory, int(len(ltmemory) * 0.9)))
        test_memory = collections.deque(itertools.islice(ltmemory, len(training_memory), len(ltmemory)))

        for i in range(config.TRAINING_LOOPS):
            minibatch = random.sample(training_memory, min(config.BATCH_SIZE, len(training_memory)))

            training_states = np.array([gameStateManager.getGameState(row['board']).flatten()[:1156] for row in minibatch])
            training_targets = np.array([row['value'] for row in minibatch])

            testminibatch = random.sample(test_memory, min(config.BATCH_SIZE, len(test_memory)))
            test_states = np.array([gameStateManager.getGameState(row['board']).flatten()[:1156] for row in testminibatch])
            test_targets = np.array([row['value'] for row in testminibatch])

            loss = None
            lossTest = None
            for t in range(config.EPOCHS):
                loss = model.train(training_states, training_targets, self.model,
                                   self.model.loss_fn, self.model.optimizer)
                lossTest = model.test(test_states, test_targets, self.model)
                self.train_overall_loss.append(round(loss, 4))
                self.test_overall_loss.append(round(lossTest, 4))

        plt.plot(self.train_overall_loss, 'k')
        plt.plot(self.test_overall_loss, 'r')

        testminibatch = random.sample(test_memory, min(config.BATCH_SIZE, len(test_memory)))
        test_states = np.array([gameStateManager.getGameState(row['board']).flatten() for row in testminibatch])
        test_targets = np.array([row['value'] for row in testminibatch])
        model.test(test_states, test_targets, self.model, printResult=True)

        plt.legend(['train_overall_loss', 'test_overall_loss'], loc='lower left')
        plt.savefig(fileName)
        display.clear_output(wait=True)
        display.display(pl.gcf())
        pl.gcf().clear()
        time.sleep(1.0)
