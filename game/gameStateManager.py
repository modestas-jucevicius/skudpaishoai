import numpy as np
import torch

R3 = 1
R4 = 2
R5 = 3
W3 = 4
W4 = 5
W5 = 6
L = 7
O = 8
B = 9
R = 10
W = 11
K = 12

switcher = {
        "R3": R3,
        "R4": R4,
        "R5": R5,
        "W3": W3,
        "W4": W4,
        "W5": W5,
        "L": L,
        "O": O,
        "B": B,
        "R": R,
        "W": W,
        "K": K
    }


def getGameState(board):
    hostLayer, guestLayer = boardToStateLayer(board)
    hostTileLayer, guestTileLayer = boardToRemainingTilesLayer(board)
    gameState = np.zeros((4, board.size, board.size), dtype=np.int32)
    gameState[0] = hostLayer
    gameState[1] = hostTileLayer
    gameState[2] = guestLayer
    gameState[3] = guestTileLayer
    # if board.playerCode == "H":
    #     gameState[4] = np.ones((board.size, board.size), dtype=np.int32)

    return gameState


def boardToStateLayer(board):
    guestLayer = np.zeros((board.size, board.size), dtype=np.int32)
    hostLayer = np.zeros((board.size, board.size), dtype=np.int32)
    for row in range(len(board.cells)):
        for col in range(len(board.cells[row])):
            if board.cells[row][col].hasTile():
                tileNumber = switcher.get(board.cells[row][col].tile.code)
                if board.cells[row][col].tile.ownerCode == "G":
                    guestLayer[row][col] = tileNumber
                elif board.cells[row][col].tile.ownerCode == "H":
                    hostLayer[row][col] = tileNumber

    return hostLayer, guestLayer


def boardToRemainingTilesLayer(board):
    remainingGuestTiles = board.tileManager.guestTiles
    remainingHostTiles = board.tileManager.hostTiles
    guestLayer = np.zeros((board.size, board.size), dtype=np.int32)
    hostLayer = np.zeros((board.size, board.size), dtype=np.int32)
    tileTypeSet = board.tileManager.getSkudTileTypes()
    for tileTypeIndex in range(len(tileTypeSet)):
        count = 0
        for tile in remainingGuestTiles:
            if tile.code == tileTypeSet[tileTypeIndex]:
                count += 1

        guestLayer[0][tileTypeIndex] = count
        count = 0
        for tile in remainingHostTiles:
            if tile.code == tileTypeSet[tileTypeIndex]:
                count += 1

        hostLayer[0][tileTypeIndex] = count

    return hostLayer, guestLayer


def getUniqueGameStateId(board):
    state = getGameState(board)
    return np.array2string(state.flatten(), max_line_width=np.inf, threshold=np.inf)



