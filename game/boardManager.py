from typing import Optional

import game.boardCell as boardCell
import game.harmonyScript as harmonyScript
import game.tileManager as tileManager
import game.notationObject as notationObject
import game.gameManager as gameManager
from game import gameStateManager, moveManager


class Board:
    def __init__(self, playerCode: str):
        self.cells = brandNew()
        self.playerCode = playerCode
        self.harmonyManager = harmonyScript.HarmonyManager()
        self.tileManager = tileManager.TileManager()
        self.size: int = 17
        self.rockLocations = []
        self.whiteLotusTiles = []
        self.winners = []
        self.id = self.getUniqueGameStateId()
        self.moveManager = moveManager.MoveManager()

    def placeTile(self, tile: tileManager.Tile, notationPoint: notationObject.NotationPoint, extraBoatPoint: Optional[
        notationObject.NotationPoint]) -> Optional[tileManager.Tile]:
        tileRemovedWithBoat = None
        if tile.isAccentTile():
            if tile.isTileType(tileManager.ROCK):
                self.placeRock(tile, notationPoint)
            elif tile.isTileType(tileManager.WHEEL):
                self.placeWheel(tile, notationPoint, False)
            elif tile.isTileType(tileManager.KNOTWEED):
                self.placeKnotweed(tile, notationPoint)
            elif tile.isTileType(tileManager.BOAT):
                tileRemovedWithBoat = self.placeBoat(tile, notationPoint, extraBoatPoint, False)
        else:
            self.putTileOnPoint(tile, notationPoint)
            if tile.isTileType(tileManager.WHITE_LOTUS):
                self.whiteLotusTiles.append(tile)
        self.flagAllTrappedAndDrainedTiles()
        self.analyzeHarmonies()
        if tile.isTileType(tileManager.BOAT):
            return tileRemovedWithBoat

    def putTileOnPoint(self, tile: tileManager.Tile, notationPoint: notationObject.NotationPoint) -> None:
        point: notationObject.RowAndColumn = notationPoint.rowAndColumn
        point: boardCell.BoardCell = self.cells[point.row][point.col]
        point.putTile(tile)

    def placeRock(self, tile: tileManager.Tile, notationPoint: notationObject.NotationPoint) -> Optional[bool]:
        rowAndCol = notationPoint.rowAndColumn
        cell: boardCell.BoardCell = self.cells[rowAndCol.row][rowAndCol.col]
        if not self.canPlaceRock(cell):
            return False
        cell.putTile(tile)
        self.rockLocations.append(rowAndCol)

    def canPlaceRock(self, cell: boardCell.BoardCell) -> bool:
        if cell.hasTile():
            return False
        if cell.isType(boardCell.GATE):
            return False
        return True

    def canPlaceWheel(self, cell: boardCell.BoardCell) -> bool:
        if cell.hasTile():
            return False
        if cell.isType(boardCell.GATE):
            return False
        rowCols = self.getSurroundingRowAndCols(cell)

        for i in range(len(rowCols)):
            bp: boardCell.BoardCell = self.cells[rowCols[i].row][rowCols[i].col]
            if bp.isType(boardCell.GATE):
                return False
            if bp.hasTile() and bp.tile.isTileType(tileManager.KNOTWEED):
                return False
            if bp.hasTile() and bp.tile.isTileType(tileManager.ROCK):
                return False
            if bp.hasTile():
                targetRowCol = self.getClockwiseRowCol(cell, rowCols[i])
                if self.isValidRowCol(targetRowCol):
                    targetBp = self.cells[targetRowCol.row][targetRowCol.col]
                    if not targetBp.canHoldTile(bp.tile, True):
                        return False
                    if targetBp.isType(boardCell.GATE):
                        return False
                else:
                    return False

        newBoard = self.getCopy()
        notationPoint = notationObject.NotationPoint(
            notationObject.RowAndColumn(cell.row, cell.col).notationPointString)
        newBoard.placeWheel(tileManager.Tile(tileManager.WHEEL, self.playerCode), notationPoint, True)
        if newBoard.moveCreatesDisharmony(cell, cell):
            return False

        return True

    def isValidRowCol(self, rowCol):
        return 0 <= rowCol.row <= 16 and 0 <= rowCol.col <= 16

    def placeWheel(self, tile: tileManager.Tile, notationPoint: notationObject.NotationPoint, ignoreCheck: bool) -> Optional[bool]:
        rowAndCol = notationPoint.rowAndColumn
        cell: boardCell.BoardCell = self.cells[rowAndCol.row][rowAndCol.col]
        rowCols = self.getSurroundingRowAndCols(rowAndCol)
        if not ignoreCheck and not self.canPlaceWheel(cell):
            return False
        cell.putTile(tile)
        results = []
        for i in range(len(rowCols)):
            savedTile = self.cells[rowCols[i].row][rowCols[i].col].removeTile()
            targetRowCol = self.getClockwiseRowCol(rowAndCol, rowCols[i])
            if self.isValidRowCol(targetRowCol):
                results.append([savedTile, targetRowCol])

        this = self
        for result in results:
            bp = this.cells[result[1].row][result[1].col]
            bp.putTile(result[0])

        self.refreshRockRowAndCols()

    def canPlaceKnotweed(self, cell: boardCell.BoardCell) -> bool:
        if cell.hasTile:
            return False

        if cell.isType(boardCell.GATE):
            return False

        return True

    def placeKnotweed(self, tile: tileManager.Tile, notationPoint: notationObject.NotationPoint) -> Optional[bool]:
        rowAndCol = notationPoint.rowAndColumn
        cell: boardCell.BoardCell = self.cells[rowAndCol.row][rowAndCol.col]

        rowCols = self.getSurroundingRowAndCols(rowAndCol)

        if not self.canPlaceKnotweed(cell):
            return False

        cell.putTile(tile)

        for i in range(len(rowCols)):
            bp = self.cells[rowCols[i].row][rowCols[i].col]
            bp.drainTile()

    def canPlaceBoat(self, cell: boardCell.BoardCell, tile: tileManager.Tile) -> bool:
        if not cell.hasTile():
            return False

        if cell.isType(boardCell.GATE):
            return False

        if cell.tile.isAccentTile():
            newBoard = self.getCopy()
            notationPoint = notationObject.NotationPoint(
                notationObject.RowAndColumn(cell.row, cell.col).notationPointString)
            newBoard.placeBoat(tileManager.Tile(tileManager.BOAT, self.playerCode), notationPoint, notationPoint, True)
            newCell = newBoard.cells[cell.row][cell.col]
            if newBoard.moveCreatesDisharmony(newCell, newCell):
                return False

        return True

    def placeBoat(self, tile: tileManager.Tile, notationPoint: notationObject.NotationPoint, extraBoatPoint: notationObject.NotationPoint, ignoreCheck: bool) -> Optional[bool]:
        rowAndCol = notationPoint.rowAndColumn
        cell: boardCell.BoardCell = self.cells[rowAndCol.row][rowAndCol.col]

        tileRemovedWithBoat = None

        if not ignoreCheck and not self.canPlaceBoat(cell, tile):
            return False

        if cell.tile.isAccentTile():
            tileRemovedWithBoat = cell.removeTile()
            if tileRemovedWithBoat.isTileType(tileManager.KNOTWEED):
                rowCols = self.getSurroundingRowAndCols(rowAndCol)
                for i in range(len(rowCols)):
                    bp = self.cells[rowCols[i].row][rowCols[i].col]
                    bp.restoreTile()
            elif tileRemovedWithBoat.isTileType(tileManager.ROCK):
                for rockLocation in self.rockLocations:
                    if rockLocation.row == rowAndCol.row and rockLocation.col == rowAndCol.col:
                        self.rockLocations.remove(rockLocation)
        else:
            bpRowCol = extraBoatPoint.rowAndColumn
            destCell = self.cells[bpRowCol.row][bpRowCol.col]
            if not destCell.canHoldTile(cell.tile, False):
                return False

            destCell.putTile(cell.removeTile())
            cell.putTile(tile)

        return tileRemovedWithBoat

    def getClockwiseRowCol(self, center, rowCol):
        if rowCol.row < center.row and rowCol.col <= center.col:
            return notationObject.RowAndColumn(rowCol.row, rowCol.col + 1)

        if rowCol.col > center.col and rowCol.row <= center.row:
            return notationObject.RowAndColumn(rowCol.row + 1, rowCol.col)

        if rowCol.row > center.row and rowCol.col >= center.col:
            return notationObject.RowAndColumn(rowCol.row, rowCol.col - 1)

        if rowCol.col < center.col and rowCol.row >= center.row:
            return notationObject.RowAndColumn(rowCol.row - 1, rowCol.col)

    def getSurroundingRowAndCols(self, rowAndCol):
        rowAndCols = []
        for row in range(rowAndCol.row - 1, rowAndCol.row + 2):
            for col in range(rowAndCol.col - 1, rowAndCol.col + 2):
                if (row != rowAndCol.row or col != rowAndCol.col) and row >= 0 and col >=0 and row < 17 and col < 17:
                    cell: boardCell.BoardCell = self.cells[row][col]
                    if not cell.isType(boardCell.NON_PLAYABLE):
                        rowAndCols.append(notationObject.RowAndColumn(row, col))

        return rowAndCols

    def refreshRockRowAndCols(self):
        self.rockLocations = []

        for row in self.cells:
            for cell in row:
                if cell.hasTile() and cell.tile.isTileType(tileManager.ROCK):
                    self.rockLocations.append(cell)

    def cellIsOpenGate(self, notationPoint: notationObject.NotationPoint):
        cell = notationPoint.rowAndColumn
        cell: boardCell.BoardCell = self.cells[cell.row][cell.col]

        return cell.isOpenGate()

    def moveTile(self, player, notationPointStart: notationObject.NotationPoint, notationPointEnd: notationObject.NotationPoint):
        startRowCol = notationPointStart.rowAndColumn
        endRowCol = notationPointEnd.rowAndColumn
        if startRowCol.row < 0 or startRowCol.row > 16 or endRowCol.row < 0 or endRowCol.row > 16:
            print("Board point doesn't exist.")
            return [False]

        cellStart: boardCell.BoardCell = self.cells[startRowCol.row][startRowCol.col]
        cellEnd: boardCell.BoardCell = self.cells[endRowCol.row][endRowCol.col]

        if not self.canMoveTileToCell(player, cellStart, cellEnd, debug=False):
            # print("Impossible to move there")
            return [False]

        tile: tileManager.Tile = cellStart.removeTile()
        capturedTile = cellEnd.tile

        if tile is None:
            print("Error: No tile to move!")
            return [False]

        cellEnd.putTile(tile)

        self.flagAllTrappedAndDrainedTiles()

        newHarmony = self.hasNewHarmony(player, tile, startRowCol, endRowCol)

        return [newHarmony, tile, capturedTile]

    def flagAllTrappedAndDrainedTiles(self):
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                bp: boardCell.BoardCell = self.cells[row][col]
                if bp.hasTile():
                    bp.tile.trapped = False
                    bp.tile.drained = False

        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                bp: boardCell.BoardCell = self.cells[row][col]
                if not bp.isType(boardCell.GATE):
                    self.trapTilesSurroundingPointIfNeeded(bp)
                    self.drainTilesSurroundingPointIfNeeded(bp)

    def drainTilesSurroundingPointIfNeeded(self, cell: boardCell.BoardCell):
        if not cell.hasTile():
            return
        if not cell.tile.isTileType(tileManager.KNOTWEED):
            return
        rowCols = self.getSurroundingRowAndCols(cell)
        for i in range(len(rowCols)):
            bp = self.cells[rowCols[i].row][rowCols[i].col]
            if bp.hasTile() and not bp.isType(boardCell.GATE) and not bp.tile.isAccentTile() and not bp.tile.isTileType(
                    tileManager.ORCHID):
                bp.tile.drained = True

    def trapTilesSurroundingPointIfNeeded(self, cell: boardCell.BoardCell):
        if not cell.hasTile():
            return
        if not cell.tile.isTileType(tileManager.ORCHID):
            return
        orchidOwner = cell.tile.ownerCode
        rowCols = self.getSurroundingRowAndCols(cell)
        for i in range(len(rowCols)):
            bp = self.cells[rowCols[i].row][rowCols[i].col]
            if bp.hasTile() and not bp.isType(boardCell.GATE) and not bp.tile.isAccentTile() and bp.tile.ownerCode != orchidOwner:
                bp.tile.trapped = True

    def whiteLotusProtected(self, lotusTile):
        return True

    def orchidCanCapture(self, orchidTile):
        orchidCanCapture = False
        for row in self.cells:
            for cell in row:
                if cell.hasTile() and cell.tile.isTileType(tileManager.WHITE_LOTUS) and cell.tile.ownerCode == orchidTile.ownerCode and not cell.isType(
                        boardCell.GATE):
                    orchidCanCapture = True

        return orchidCanCapture

    def orchidVulnerable(self, orchidTile):
        orchidVulnerable = False
        for lotus in self.whiteLotusTiles:
            if lotus.ownerCode == orchidTile.ownerCode:
                orchidVulnerable = True

        return orchidVulnerable

    def canCapture(self, cellStart: boardCell.BoardCell, cellEnd: boardCell.BoardCell):
        tile: tileManager.Tile = cellStart.tile
        otherTile: tileManager.Tile = cellEnd.tile

        if tile.ownerCode == otherTile.ownerCode:
            return False

        if tile.isTileType(tileManager.ORCHID) and not otherTile.isAccentTile():
            return self.orchidCanCapture(tile)

        if tile.clashesWith(otherTile):
            return True

        if otherTile.isTileType(tileManager.ORCHID) and not tile.isAccentTile():
            if self.orchidVulnerable(otherTile):
                return True

        return False

    def canMoveTileToCell(self, player, cellStart: boardCell.BoardCell, cellEnd: boardCell.BoardCell, debug=False):
        if not cellStart.hasTile():
            if debug:
                print("cellstart Has tile")
            return False

        if cellStart.tile.ownerCode != player:
            if debug:
                print("cellstart wrong owner")
            return False

        if cellStart.tile.trapped:
            if debug:
                print("cellstart trapped")
            return False

        if cellStart.tile.drained:
            if debug:
                print("cellstart drained")
            return False

        if cellEnd.isType(boardCell.GATE):
            if debug:
                print("cellend gate")
            return False

        if not cellEnd.canHoldTile(cellStart.tile, False):
            if debug:
                print("cellend cant hold tile")
            return False

        canCapture = False
        if cellEnd.hasTile():
            canCapture = self.canCapture(cellStart, cellEnd)

        if cellEnd.hasTile() and not canCapture:
            if debug:
                print("can't capture")
            return False

        if not cellEnd.canHoldTile(cellStart.tile, canCapture):
            if debug:
                print("can't hold tile after capture")
            return False

        numMoves = cellStart.tile.getMoveDistance()
        if abs(cellStart.row - cellEnd.row) + abs(cellStart.col - cellEnd.col) > numMoves:
            if debug:
                print("too far away")
            return False
        else:
            if not self.verifyAbleToReach(cellStart, cellEnd, numMoves):
                if debug:
                    print("too far away (verify)")
                return False

        if self.moveCreatesDisharmony(cellStart, cellEnd):
            if debug:
                print("disharmony")
            return False

        return True

    def canTransportTileToCellWithBoat(self, cellStart: boardCell.BoardCell, cellEnd: boardCell.BoardCell):
        if not cellStart.hasTile():
            return False

        if cellEnd.isType(boardCell.GATE):
            return False

        if cellEnd.hasTile():
            return False

        if not cellEnd.canHoldTile(cellStart.tile, False):
            return False

        newBoard = self.getCopy()
        newBoardCellStart = newBoard.cells[cellStart.row][cellStart.col]
        notationPoint = notationObject.NotationPoint(
            notationObject.RowAndColumn(cellStart.row, cellStart.col).notationPointString)
        notationPointEnd = notationObject.NotationPoint(
            notationObject.RowAndColumn(cellEnd.row, cellEnd.col).notationPointString)
        newBoard.placeBoat(tileManager.Tile(tileManager.BOAT, self.playerCode), notationPoint, notationPointEnd, True)
        if newBoard.moveCreatesDisharmony(newBoardCellStart, newBoardCellStart):
            return False

        return True

    def moveCreatesDisharmony(self, cellStart: boardCell.BoardCell, cellEnd: boardCell.BoardCell):
        endTile = None
        if cellStart.row != cellEnd.row or cellStart.col != cellEnd.col:
            endTile = cellEnd.removeTile()
            cellEnd.putTile(cellStart.removeTile())

        clashFound = False

        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                cell = self.cells[row][col]
                if cell.hasTile():
                    if self.hasDisharmony(cell):
                        clashFound = True
                        break

        if cellStart.row != cellEnd.row or cellStart.col != cellEnd.col:
            cellStart.putTile(cellEnd.removeTile())
            cellEnd.putTile(endTile)

        return clashFound

    def verifyAbleToReach(self, cellStart: boardCell.BoardCell, cellEnd: boardCell.BoardCell, numMoves):
        return self.pathFound(cellStart, cellEnd, numMoves)

    def pathFound(self, cellStart: boardCell.BoardCell, cellEnd: boardCell.BoardCell, numMoves):
        if cellStart.isType(boardCell.NON_PLAYABLE) or cellEnd.isType(boardCell.NON_PLAYABLE):
            return False

        if cellStart.row == cellEnd.row and cellStart.col == cellEnd.col:
            return True

        if numMoves <= 0:
            return False

        minMoves = abs(cellStart.row - cellEnd.row) + abs(cellStart.col - cellEnd.col)
        if minMoves == 1:
            return True

        nextRow = cellStart.row - 1
        if nextRow >= 0:
            nextPoint = self.cells[nextRow][cellStart.col]
            if not nextPoint.hasTile() and self.pathFound(nextPoint, cellEnd, numMoves - 1):
                return True

        nextRow = cellStart.row + 1
        if nextRow < 17:
            nextPoint = self.cells[nextRow][cellStart.col]
            if not nextPoint.hasTile() and self.pathFound(nextPoint, cellEnd, numMoves - 1):
                return True

        nextCol = cellStart.col - 1
        if nextCol >= 0:
            nextPoint = self.cells[cellStart.row][nextCol]
            if not nextPoint.hasTile() and self.pathFound(nextPoint, cellEnd, numMoves - 1):
                return True

        nextCol = cellStart.col + 1
        if nextCol < 17:
            nextPoint = self.cells[cellStart.row][nextCol]
            if not nextPoint.hasTile() and self.pathFound(nextPoint, cellEnd, numMoves - 1):
                return True

        return False

    def rowBlockedByRock(self, rowNum):
        blocked = False
        for rowAndCol in self.rockLocations:
            if rowAndCol.row == rowNum:
                blocked = True

        return blocked

    def columnBlockedByRock(self, colNum):
        blocked = False
        for rowAndCol in self.rockLocations:
            if rowAndCol.col == colNum:
                blocked = True

        return blocked

    def markSpacesBetweenHarmonies(self):
        for row in self.cells:
            for cell in row:
                cell.betweenHarmony = False
                cell.betweenHarmonyHost = False
                cell.betweenHarmonyGuest = False

        for harmony in self.harmonyManager.harmonies:
            if harmony.tile1Pos.row == harmony.tile2Pos.row:
                row = harmony.tile1Pos.row
                firstCol = harmony.tile1Pos.col
                lastCol = harmony.tile2Pos.col
                if harmony.tile2Pos.col < harmony.tile1Pos.col:
                    firstCol = harmony.tile2Pos.col
                    lastCol = harmony.tile1Pos.col
                for col in range(firstCol + 1, lastCol):
                    self.cells[row][col].betweenHarmony = True
                    if harmony.hasOwner(gameManager.GUEST):
                        self.cells[row][col].betweenHarmonyGuest = True
                    if harmony.hasOwner(gameManager.HOST):
                        self.cells[row][col].betweenHarmonyHost = True
            elif harmony.tile1Pos.col == harmony.tile2Pos.col:
                col = harmony.tile1Pos.col
                firstRow = harmony.tile1Pos.row
                lastRow = harmony.tile2Pos.row
                if harmony.tile2Pos.row < harmony.tile1Pos.row:
                    firstRow = harmony.tile2Pos.row
                    lastRow = harmony.tile1Pos.row
                for row in range(firstRow + 1, lastRow):
                    self.cells[row][col].betweenHarmony = True
                    if harmony.hasOwner(gameManager.GUEST):
                        self.cells[row][col].betweenHarmonyGuest = True
                    if harmony.hasOwner(gameManager.HOST):
                        self.cells[row][col].betweenHarmonyHost = True

    def analyzeHarmonies(self):
        self.harmonyManager.clearList()
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                cell: boardCell.BoardCell = self.cells[row][col]
                if cell.hasTile():
                    tileHarmonies = self.getTileHarmonies(cell)
                    self.harmonyManager.addHarmonies(tileHarmonies)
                    cell.tile.harmonyOwners = []
                    for i in range(len(tileHarmonies)):
                        for j in range(len(tileHarmonies[i].owners)):
                            harmonyOwnerCode = tileHarmonies[i].owners[j]
                            harmonyTile1 = tileHarmonies[i].tile1
                            harmonyTile2 = tileHarmonies[i].tile2
                            if harmonyOwnerCode not in harmonyTile1.harmonyOwners:
                                harmonyTile1.harmonyOwners.append(harmonyOwnerCode)

                            if harmonyOwnerCode not in harmonyTile2.harmonyOwners:
                                harmonyTile2.harmonyOwners.append(harmonyOwnerCode)
        self.markSpacesBetweenHarmonies()

        self.winners = []
        harmonyRingOwners = self.harmonyManager.harmonyRingExists()
        for player in harmonyRingOwners:
            if player not in self.winners:
                self.winners.append(player)

    def getTileHarmonies(self, cell: boardCell.BoardCell):
        tile: tileManager.Tile = cell.tile
        rowAndCol = cell
        tileHarmonies = []
        if self.cells[rowAndCol.row][rowAndCol.col].isType(boardCell.GATE):
            return tileHarmonies

        if not self.rowBlockedByRock(rowAndCol.row):
            leftHarmony = self.getHarmonyLeft(tile, rowAndCol)
            if leftHarmony is not None:
                tileHarmonies.append(leftHarmony)

            rightHarmony = self.getHarmonyRight(tile, rowAndCol)
            if rightHarmony is not None:
                tileHarmonies.append(rightHarmony)

        if not self.columnBlockedByRock(rowAndCol.col):
            upHarmony = self.getHarmonyUp(tile, rowAndCol)
            if upHarmony is not None:
                tileHarmonies.append(upHarmony)

            downHarmony = self.getHarmonyDown(tile, rowAndCol)
            if downHarmony is not None:
                tileHarmonies.append(downHarmony)

        return tileHarmonies

    def getHarmonyLeft(self, tile: tileManager.Tile, endRowCol):
        colToCheck = endRowCol.col - 1
        while colToCheck >= 0 and not self.cells[endRowCol.row][colToCheck].hasTile() and not self.cells[endRowCol.row][colToCheck].isType(
                boardCell.GATE):
            colToCheck = colToCheck - 1

        if colToCheck >= 0:
            checkPoint = self.cells[endRowCol.row][colToCheck]
            if not checkPoint.isType(boardCell.GATE) and tile.formsHarmonyWith(checkPoint.tile):
                return harmonyScript.Harmony(tile, endRowCol, checkPoint.tile, notationObject.RowAndColumn(endRowCol.row, colToCheck))

    def getHarmonyRight(self, tile: tileManager.Tile, endRowCol):
        colToCheck = endRowCol.col + 1
        while colToCheck <= 16 and not self.cells[endRowCol.row][colToCheck].hasTile() and not self.cells[endRowCol.row][colToCheck].isType(
                boardCell.GATE):
            colToCheck = colToCheck + 1

        if colToCheck <= 16:
            checkPoint = self.cells[endRowCol.row][colToCheck]
            if not checkPoint.isType(boardCell.GATE) and tile.formsHarmonyWith(checkPoint.tile):
                return harmonyScript.Harmony(tile, endRowCol, checkPoint.tile, notationObject.RowAndColumn(endRowCol.row, colToCheck))

    def getHarmonyUp(self, tile: tileManager.Tile, endRowCol):
        rowToCheck = endRowCol.row - 1
        while rowToCheck >= 0 and not self.cells[rowToCheck][endRowCol.col].hasTile() and not self.cells[rowToCheck][endRowCol.col].isType(
                boardCell.GATE):
            rowToCheck = rowToCheck - 1

        if rowToCheck >= 0:
            checkPoint = self.cells[rowToCheck][endRowCol.col]
            if not checkPoint.isType(boardCell.GATE) and tile.formsHarmonyWith(checkPoint.tile):
                return harmonyScript.Harmony(tile, endRowCol, checkPoint.tile, notationObject.RowAndColumn(rowToCheck, endRowCol.col))

    def getHarmonyDown(self, tile: tileManager.Tile, endRowCol):
        rowToCheck = endRowCol.row + 1
        while rowToCheck <= 16 and not self.cells[rowToCheck][endRowCol.col].hasTile() and not self.cells[rowToCheck][endRowCol.col].isType(
                boardCell.GATE):
            rowToCheck = rowToCheck + 1

        if rowToCheck <= 16:
            checkPoint = self.cells[rowToCheck][endRowCol.col]
            if not checkPoint.isType(boardCell.GATE) and tile.formsHarmonyWith(checkPoint.tile):
                return harmonyScript.Harmony(tile, endRowCol, checkPoint.tile, notationObject.RowAndColumn(rowToCheck, endRowCol.col))

    def hasNewHarmony(self, player, tile: tileManager.Tile, startRowCol, endRowCol):
        oldHarmonies = self.harmonyManager.harmonies
        self.analyzeHarmonies()

        return self.harmonyManager.hasNewHarmony(player, oldHarmonies)

    def hasDisharmony(self, cell: boardCell.BoardCell):
        if cell.isType(boardCell.GATE):
            return False

        tile: tileManager.Tile = cell.tile

        if self.hasDisharmonyLeft(tile, cell):
            return True

        if self.hasDisharmonyRight(tile, cell):
            return True

        if self.hasDisharmonyUp(tile, cell):
            return True

        if self.hasDisharmonyDown(tile, cell):
            return True

        return False

    def hasDisharmonyLeft(self, tile: tileManager.Tile, endRowCol):
        colToCheck = endRowCol.col - 1
        while colToCheck >= 0 and not self.cells[endRowCol.row][colToCheck].hasTile() and not self.cells[endRowCol.row][colToCheck].isType(
                boardCell.GATE):
            colToCheck = colToCheck - 1

        if colToCheck >= 0:
            checkPoint: boardCell.BoardCell = self.cells[endRowCol.row][colToCheck]
            return not checkPoint.isType(boardCell.GATE) and tile.clashesWith(checkPoint.tile)

    def hasDisharmonyRight(self, tile: tileManager.Tile, endRowCol):
        colToCheck = endRowCol.col + 1
        while colToCheck <= 16 and not self.cells[endRowCol.row][colToCheck].hasTile() and not self.cells[endRowCol.row][colToCheck].isType(
                boardCell.GATE):
            colToCheck = colToCheck + 1

        if colToCheck <= 16:
            checkPoint: boardCell.BoardCell = self.cells[endRowCol.row][colToCheck]
            return not checkPoint.isType(boardCell.GATE) and tile.clashesWith(checkPoint.tile)

    def hasDisharmonyUp(self, tile: tileManager.Tile, endRowCol):
        rowToCheck = endRowCol.row - 1
        while rowToCheck >= 0 and not self.cells[rowToCheck][endRowCol.col].hasTile() and not self.cells[rowToCheck][endRowCol.col].isType(
                boardCell.GATE):
            rowToCheck = rowToCheck - 1

        if rowToCheck >= 0:
            checkPoint: boardCell.BoardCell = self.cells[rowToCheck][endRowCol.col]
            return not checkPoint.isType(boardCell.GATE) and tile.clashesWith(checkPoint.tile)

    def hasDisharmonyDown(self, tile: tileManager.Tile, endRowCol):
        rowToCheck = endRowCol.row + 1
        while rowToCheck <= 16 and not self.cells[rowToCheck][endRowCol.col].hasTile() and not self.cells[rowToCheck][endRowCol.col].isType(
                boardCell.GATE):
            rowToCheck = rowToCheck + 1

        if rowToCheck <= 16:
            checkPoint: boardCell.BoardCell = self.cells[rowToCheck][endRowCol.col]
            return not checkPoint.isType(boardCell.GATE) and tile.clashesWith(checkPoint.tile)

    def setPossibleMoveCells(self, cellStart: boardCell.BoardCell):
        if not cellStart.hasTile:
            return

        player = cellStart.tile.ownerCode
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                self.cells[row][col].removeType(boardCell.POSSIBLE_MOVE)
                if self.canMoveTileToCell(player, cellStart, self.cells[row][col]):
                    self.cells[row][col].addType(boardCell.POSSIBLE_MOVE)

    def getPossibleMoveCells(self, cellStart: boardCell.BoardCell):
        if not cellStart.hasTile:
            return []
        cells = []
        player = cellStart.tile.ownerCode
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                if self.canMoveTileToCell(player, cellStart, self.cells[row][col]):
                    cells.append(self.cells[row][col])

        return cells

    def removePossibleMoveCells(self):
        for row in self.cells:
            for cell in row:
                cell.removeType(boardCell.POSSIBLE_MOVE)

    def setOpenGatePossibleMoves(self, player, tile: tileManager.Tile):
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                bp = self.cells[row][col]
                if bp.isOpenGate():
                    self.cells[row][col].addType(boardCell.POSSIBLE_MOVE)

    def playerControlsLessThanTwoGates(self, player):
        count = 0
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                bp: boardCell.BoardCell = self.cells[row][col]
                if bp.isType(boardCell.GATE) and bp.hasTile() and bp.tile.ownerCode == player:
                    count = count + 1

        return count < 2

    def playerHasNoGrowingFlowers(self, player):
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                bp: boardCell.BoardCell = self.cells[row][col]
                if bp.isType(boardCell.GATE) and bp.hasTile() and bp.tile.ownerCode == player:
                    return False

        return True

    def revealSpecialFlowerPlacementCells(self, player):
        bpCheckList = []
        row = 0
        col = 8
        bp: boardCell.BoardCell = self.cells[row][col]
        if bp.hasTile() and bp.tile.ownerCode == player:
            bpCheckList.append(self.cells[row][col - 1])
            bpCheckList.append(self.cells[row][col + 1])

        row = 16
        bp: boardCell.BoardCell = self.cells[row][col]
        if bp.hasTile() and bp.tile.ownerCode == player:
            bpCheckList.append(self.cells[row][col - 1])
            bpCheckList.append(self.cells[row][col + 1])

        row = 8
        col = 0
        bp: boardCell.BoardCell = self.cells[row][col]
        if bp.hasTile() and bp.tile.ownerCode == player:
            bpCheckList.append(self.cells[row][col - 1])
            bpCheckList.append(self.cells[row][col + 1])

        col = 16
        bp: boardCell.BoardCell = self.cells[row][col]
        if bp.hasTile() and bp.tile.ownerCode == player:
            bpCheckList.append(self.cells[row][col - 1])
            bpCheckList.append(self.cells[row][col + 1])

        for bp in bpCheckList:
            if not bp.hasTile():
                bp.addType(boardCell.POSSIBLE_MOVE)

    def setGuestGateOpen(self):
        row = 16
        col = 8
        if self.cells[row][col].isOpenGate():
            self.cells[row][col].addType(boardCell.POSSIBLE_MOVE)

    def revealPossiblePlacementCells(self, tile: tileManager.Tile):
        for row in self.cells:
            for cell in row:
                if cell.isType(boardCell.NON_PLAYABLE):
                    continue

                valid = False
                if (tile.isTileType(tileManager.ROCK) and self.canPlaceRock(cell)) or (tile.isTileType(
                        tileManager.WHEEL) and self.canPlaceWheel(cell)) or (tile.isTileType(
                        tileManager.KNOTWEED) and self.canPlaceKnotweed(cell)) or (tile.isTileType(
                        tileManager.BOAT) and self.canPlaceBoat(cell, tile)):
                    cell.addType(boardCell.POSSIBLE_MOVE)

    def revealBoatBonusCells(self, cell: boardCell.BoardCell):
        if not cell.hasTile:
            return

        rowCols = self.getSurroundingRowAndCols(cell)
        for i in range(len(rowCols)):
            cellEnd = self.cells[rowCols[i].row][rowCols[i].col]
            if self.canTransportTileToCellWithBoat(cell, cellEnd):
                cellEnd.addType(boardCell.POSSIBLE_MOVE)

    def getCopy(self):
        copyBoard = Board(self.playerCode)
        copyBoard.tileManager = self.tileManager.getCopy()
        copyBoard.moveManager = self.moveManager.getCopy()
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                copyBoard.cells[row][col] = self.cells[row][col].getCopy()

        for i in range(len(self.whiteLotusTiles)):
            copyBoard.whiteLotusTiles.append(self.whiteLotusTiles[i].getCopy())

        copyBoard.refreshRockRowAndCols()
        copyBoard.analyzeHarmonies()

        return copyBoard

    def numTilesInGardensForPlayer(self, player):
        count = 0
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                bp: boardCell.BoardCell = self.cells[row][col]
                if len(bp.types) == 1 and bp.hasTile():
                    if bp.isType(bp.tile.getBasicColorCode() and bp.tile.ownerCode == player):
                        count = count + 1

        return count

    def numTilesOnBoardForPlayer(self, player):
        count = 0
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                bp: boardCell.BoardCell = self.cells[row][col]
                if bp.hasTile() and bp.tile.ownerCode == player:
                    count = count + 1

        return count

    def getSurroundness(self, player):
        up = 0
        hasUp = False
        down = 0
        hasDown = False
        left = 0
        hasLeft = False
        right = 0
        hasRight = False
        for row in range(len(self.cells)):
            for col in range(len(self.cells[row])):
                bp: boardCell.BoardCell = self.cells[row][col]
                if bp.hasTile() and bp.tile.ownerCode == player:
                    if bp.row > 8:
                        down = down + 1
                        hasDown = True

                    if bp.row < 8:
                        up = up + 1
                        hasUp = True

                    if bp.col < 8:
                        left = left + 1
                        hasLeft = True

                    if bp.col > 8:
                        right = right + 1
                        hasRight = True

        lowest = min(up, down, left, right)
        if lowest == 0:
            return hasUp + hasDown + hasLeft + hasRight
        else:
            return lowest * 4

    def getUniqueGameStateId(self):
        self.id = gameStateManager.getUniqueGameStateId(self)
        return self.id


def brandNew():
    cells = []
    cells.append([
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.gate(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable()
    ])
    cells.append([
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.redWhiteNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable()
    ])
    cells.append([
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.whiteNeutral(),
        boardCell.redWhiteNeutral(),
        boardCell.redNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable()
    ])
    cells.append([
        boardCell.nonPlayable(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.whiteNeutral(),
        boardCell.white(),
        boardCell.redWhite(),
        boardCell.red(),
        boardCell.redNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.nonPlayable()
    ])
    cells.append([
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.whiteNeutral(),
        boardCell.white(),
        boardCell.white(),
        boardCell.redWhite(),
        boardCell.red(),
        boardCell.red(),
        boardCell.redNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral()
    ])
    cells.append([
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.whiteNeutral(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.redWhite(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.redNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral()
    ])
    cells.append([
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.whiteNeutral(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.redWhite(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.redNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral()
    ])
    cells.append([
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.whiteNeutral(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.redWhite(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.redNeutral(),
        boardCell.neutral(),
        boardCell.neutral()
    ])
    cells.append([
        boardCell.gate(),
        boardCell.redWhiteNeutral(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhite(),
        boardCell.redWhiteNeutral(),
        boardCell.gate()
    ])
    cells.append([
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.redNeutral(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.redWhite(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.whiteNeutral(),
        boardCell.neutral(),
        boardCell.neutral()
    ])
    cells.append([
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.redNeutral(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.redWhite(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.whiteNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral()
    ])
    cells.append([
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.redNeutral(),
        boardCell.red(),
        boardCell.red(),
        boardCell.red(),
        boardCell.redWhite(),
        boardCell.white(),
        boardCell.white(),
        boardCell.white(),
        boardCell.whiteNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral()
    ])
    cells.append([
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.redNeutral(),
        boardCell.red(),
        boardCell.red(),
        boardCell.redWhite(),
        boardCell.white(),
        boardCell.white(),
        boardCell.whiteNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral()
    ])
    cells.append([
        boardCell.nonPlayable(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.redNeutral(),
        boardCell.red(),
        boardCell.redWhite(),
        boardCell.white(),
        boardCell.whiteNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.nonPlayable()
    ])
    cells.append([
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.redNeutral(),
        boardCell.redWhite(),
        boardCell.whiteNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable()
    ])
    cells.append([
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.redWhiteNeutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable()
    ])
    cells.append([
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.gate(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.neutral(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable(),
        boardCell.nonPlayable()
    ])

    for row in range(len(cells)):
        for col in range(len(cells[row])):
            cells[row][col].row = row
            cells[row][col].col = col

    return cells
