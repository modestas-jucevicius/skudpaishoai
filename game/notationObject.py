from __future__ import annotations

import game.gameManager as gameManager

PLANTING = "Planting"
ARRANGING = "Arranging"
BRAND_NEW = "BrandNew"
WAITING_FOR_ENDPOINT = "WaitingForEndpoint"


class RowAndColumn:
    def __init__(self, row: int, col: int):
        self.row = row
        self.col = col
        self.x = col - 8
        self.y = 8 - row
        self.notationPointString = str(self.x) + "," + str(self.y)

    def samesies(self, other: RowAndColumn) -> bool:
        return self.row == other.row and self.col == other.col

    def getNotationPoint(self) -> NotationPoint:
        return NotationPoint(self.notationPointString)


class NotationPoint:
    def __init__(self, text: str):
        self.pointText = text
        parts = text.split(',')
        self.x = int(parts[0])
        self.y = int(parts[1])
        col = self.x + 8
        row = abs(self.y - 8)
        self.rowAndColumn = RowAndColumn(row, col)

    def samesies(self, other: NotationPoint) -> bool:
        return self.x == other.x and self.y == other.y

    def toArr(self) -> list:
        return [self.x, self.y]


class NotationMove:
    def __init__(self, text):
        self.fullMoveText = text
        self.valid = True
        self.moveNum = None
        self.playerCode = None
        self.player = None
        self.moveTextOnly = None
        self.accentTiles = None
        self.moveType = None
        self.plantedFlowerType = None
        self.endPoint = None
        self.startPoint = None
        self.bonusTileCode = None
        self.boatBonusPoint = None
        self.bonusEndPoint = None
        self.analyzeMove()

    def analyzeMove(self):
        parts = self.fullMoveText.split('.')
        moveNumAndPlayer: str = parts[0]
        self.moveNum = int(moveNumAndPlayer[0:-1])
        self.playerCode = moveNumAndPlayer[-1]
        if self.playerCode == gameManager.GUEST:
            self.player = gameManager.GUEST
        elif self.playerCode == gameManager.HOST:
            self.player = gameManager.HOST

        if len(parts) < 2:
            return

        moveText = parts[1]
        self.moveTextOnly = moveText

        if self.moveNum == 0:
            self.accentTiles = moveText.split(',')
            return

        char0 = moveText[0]
        if char0 == 'R' or char0 == 'W':
            self.moveType = PLANTING
        elif char0 == '(':
            self.moveType = ARRANGING

        if self.moveType == PLANTING:
            char1 = moveText[1]
            self.plantedFlowerType = char0 + char1
            if moveText[2] != '(':
                self.valid = False

            if moveText[-1] == ')':
                self.endPoint = NotationPoint(moveText[moveText.index('(')+1:moveText.index(')')])
            else:
                self.valid = False
        elif self.moveType == ARRANGING:
            parts = moveText[moveText.index('(')+1:].split(')-(')
            self.startPoint = NotationPoint(parts[0])
            self.endPoint = NotationPoint(parts[1][0:parts[1].index(')')])
            if '+' in parts[1] or '_' in parts[1]:
                bonusChar = '+'
                if '_' in parts[1]:
                    bonusChar = '_'

                bonus = parts[1][parts[1].index(bonusChar)+1:]
                self.bonusTileCode = bonus[0:bonus.index('(')]

                if len(parts) > 2:
                    self.bonusEndPoint = NotationPoint(bonus[bonus.index('(')+1:])
                    self.boatBonusPoint = NotationPoint(parts[2][0: parts[2].index(')')])
                else:
                    self.bonusEndPoint = NotationPoint(bonus[bonus.index('(')+1:bonus.index(')')])

        if char0 in ["L", "O"] or moveText[1] == "(":
            self.moveType = PLANTING
            self.plantedFlowerType = char0
            if moveText[2] != '(':
                self.valid = False

            if moveText[-1] == ')':
                self.endPoint = NotationPoint(moveText[moveText.index('(')+1:moveText.index(')')])
            else:
                self.valid = False
            if ")-(" in moveText:
                parts = moveText[moveText.index('(') + 1:].split(')-(')
                self.boatBonusPoint = NotationPoint(parts[-1][0: parts[-1].index(')')])




    def hasHarmonyBonus(self):
        return self.bonusTileCode is not None

    def isValidNotation(self):
        return self.valid

    def equals(self, otherMove):
        return self.fullMoveText == otherMove.fullMoveText

    def getCopy(self):
        return NotationMove(self.fullMoveText)




class NotationBuilder:
    def __init__(self):
        self.moveType = None
        self.plantedFlowerType = None
        self.endPoint = None
        self.startPoint = None
        self.bonusTileCode = None
        self.bonusEndPoint = None
        self.boatBonusPoint = None
        self.status = BRAND_NEW

    def getFirstMoveForHost(self, tileCode):
        builder = NotationBuilder()
        builder.moveType = PLANTING
        builder.plantedFlowerType = tileCode
        builder.endPoint = NotationPoint("0,8")
        return builder

    def getNotationMove(self, moveNum, player):
        bonusChar = '+'
        notationLine = str(moveNum) + player[0] + '.'
        if self.moveType == ARRANGING:
            notationLine += '(' + self.startPoint.pointText + ")-(" + self.endPoint.pointText + ')'
            if self.bonusTileCode is not None and self.bonusEndPoint is not None:
                notationLine += bonusChar + self.bonusTileCode + '(' + self.bonusEndPoint.pointText + ')'
                if self.boatBonusPoint is not None:
                    notationLine += "-(" + self.boatBonusPoint.pointText + ")"
        elif self.moveType == PLANTING:
            notationLine += self.plantedFlowerType + '(' + self.endPoint.pointText + ')'
            if self.boatBonusPoint is not None:
                notationLine += "-(" + self.boatBonusPoint.pointText + ")"

        return NotationMove(notationLine)

    def getCopy(self):
        copy = NotationBuilder()
        copy.moveType = self.moveType
        copy.plantedFlowerType = self.plantedFlowerType
        copy.endPoint = self.endPoint
        copy.startPoint = self.startPoint
        copy.bonusTileCode = self.bonusTileCode
        copy.bonusEndPoint = self.bonusEndPoint
        copy.boatBonusPoint = self.boatBonusPoint
        copy.status = self.status
        return copy






