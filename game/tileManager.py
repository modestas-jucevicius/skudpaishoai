from __future__ import annotations

import config
import game.gameManager as gameManager

RED = "R"
WHITE = "W"
ROCK = 'R'
WHEEL = 'W'
KNOTWEED = 'K'
BOAT = 'B'
WHITE_LOTUS = 'L'
ORCHID = 'O'

tileId = 1


class Tile:
    def __init__(self, code: str, ownerCode: str):
        self.code = code
        self.ownerCode = ownerCode
        self.basicFlower: bool = False
        self.drained: bool = False
        self.trapped: bool = False
        self.harmonyOwners = []
        if len(code) > 1:
            self.basicFlower = True

    def isTileType(self, tileType: str) -> bool:
        return self.code == tileType

    def getMoveDistance(self) -> int:
        if self.basicFlower:
            return self.getBasicValue()
        if self.code == WHITE_LOTUS:
            return 2
        if self.code == ORCHID:
            return 6
        return 0

    def formsHarmonyWith(self, otherTile: Tile) -> bool:
        if not self.isBasicFlowerOrWhiteLotus() or not (otherTile.isBasicFlowerOrWhiteLotus()):
            return False
        if (self.code == WHITE_LOTUS and not otherTile.isBasicFlower()) or (otherTile.isTileType(WHITE_LOTUS) and self.basicFlower):
            return False
        if self.drained or otherTile.drained:
            return False
        if (self.code == WHITE_LOTUS and otherTile.isBasicFlower()) or (otherTile.isTileType(WHITE_LOTUS) and self.basicFlower):
            return True
        if self.ownerCode != Tile.getOwnerCode(otherTile):
            return False
        if self.getBasicColorCode() == otherTile.getBasicColorCode() and abs(self.getBasicValue() - otherTile.getBasicValue()) == 1:
            return True
        if self.getBasicColorCode() != otherTile.getBasicColorCode() and abs(self.getBasicValue() - otherTile.getBasicValue()) == 2:
            return True

    def clashesWith(self, otherTile: Tile) -> bool:
        return self.basicFlower and otherTile.isBasicFlower() and self.getBasicColorCode() != otherTile.getBasicColorCode() and self.getBasicValue() == otherTile.getBasicValue()

    def isBasicFlower(self) -> bool:
        return self.basicFlower

    def isBasicFlowerOrWhiteLotus(self) -> bool:
        return self.basicFlower or self.code == WHITE_LOTUS

    def getOwnerCode(self) -> str:
        return self.ownerCode

    def getBasicColorCode(self) -> str:
        return self.code[0]

    def getBasicValue(self) -> int:
        return int(self.code[1])

    def getCopy(self) -> Tile:
        return Tile(self.code, self.ownerCode)

    def isSpecialFlower(self) -> bool:
        return self.code == WHITE_LOTUS or self.code == ORCHID

    def isAccentTile(self) -> bool:
        return self.code == ROCK or self.code == WHEEL or self.code == KNOTWEED or self.code == BOAT

    def drain(self) -> None:
        if self.basicFlower:
            self.drained = True

    def restore(self) -> None:
        self.drained = False

    def getClashTileCode(self, tileCode):
        if len(tileCode) == 2:
            if tileCode[0] == RED:
                return WHITE + tileCode[1]

            if tileCode[0] == WHITE:
                return RED + tileCode[1]




class TileManager:
    def __init__(self):
        self.hostTiles = self.loadTileSet(gameManager.HOST)
        self.guestTiles = self.loadTileSet(gameManager.GUEST)
        self.remainingAccentTilesHost = 8
        self.remainingAccentTilesGuest = 8

    def loadTileSet(self, ownerCode):
        return self.loadSkudSet(ownerCode)

    def loadSkudSet(self, ownerCode):
        tiles = []
        for i in range(2):
            tiles.append(Tile(ROCK, ownerCode))
            tiles.append(Tile(WHEEL, ownerCode))
            tiles.append(Tile(KNOTWEED, ownerCode))
            tiles.append(Tile(BOAT, ownerCode))

        for i in range(config.SIMPLE_FLOWER_TILE_NUMBER):
            tiles.append(Tile("R3", ownerCode))
            tiles.append(Tile("R4", ownerCode))
            tiles.append(Tile("R5", ownerCode))
            tiles.append(Tile("W3", ownerCode))
            tiles.append(Tile("W4", ownerCode))
            tiles.append(Tile("W5", ownerCode))

        tiles.append(Tile(WHITE_LOTUS, ownerCode))
        tiles.append(Tile(ORCHID, ownerCode))
        return tiles

    def getSkudTileTypes(self):
        return [ROCK, WHEEL, KNOTWEED, BOAT, "R3", "R4", "R5", "W3", "W4", "W5", WHITE_LOTUS, ORCHID]

    def grabTile(self, player, tileCode):
        tilePile = self.hostTiles
        if player == gameManager.GUEST:
            tilePile = self.guestTiles

        tile = None
        for i in range(len(tilePile)):
            if tilePile[i].code == tileCode:
                tile = tilePile.pop(i)
                if tile.isAccentTile():
                    if player == gameManager.GUEST:
                        self.remainingAccentTilesGuest -= 1
                    if player == gameManager.HOST:
                        self.remainingAccentTilesHost -= 1

                break

        if tile is None:
            print("NONE OF THAT TILE FOUND")

        return tile

    def putTileBack(self, tile):
        player = tile.ownerCode
        tilePile = self.hostTiles
        if player == gameManager.GUEST:
            tilePile = self.guestTiles

        tilePile.append(tile)

    def aPlayerIsOutOfBasicFlowerTiles(self):
        hostHasBasic = False
        for tile in self.hostTiles:
            if tile.isBasicFlower():
                hostHasBasic = True
                break

        guestHasBasic = False
        for tile in self.guestTiles:
            if tile.isBasicFlower():
                guestHasBasic = True
                break

        if not hostHasBasic and not guestHasBasic:
            return "BOTH PLAYERS"

        if not hostHasBasic:
            return gameManager.HOST

        if not guestHasBasic:
            return gameManager.GUEST

    def getPlayerWithMoreAccentTiles(self):
        hostCount = 0
        for tile in self.hostTiles:
            if tile.isAccentTile():
                hostCount += 1

        guestCount = 0
        for tile in self.guestTiles:
            if tile.isAccentTile():
                guestCount += 1

        if hostCount > guestCount:
            return gameManager.HOST

        if guestCount > hostCount:
            return gameManager.GUEST

    def playerHasBothSpecialTilesRemaining(self, player):
        tilePile = self.hostTiles
        if player == gameManager.GUEST:
            tilePile = self.guestTiles

        specialTileCount = 0

        for tile in tilePile:
            if tile.isSpecialFlower():
                specialTileCount += 1

        return specialTileCount > 1

    def getCopy(self):
        copy = TileManager()
        copy.hostTiles = self.hostTiles.copy()
        copy.guestTiles = self.guestTiles.copy()

        return copy








