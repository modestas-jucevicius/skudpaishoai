from __future__ import annotations

import game.gameManager as gameManager
import game.tileManager as tileManager
import game.notationObject as notationObject


class Harmony:
    def __init__(self, tile1: tileManager.Tile, tile1RowAndColumn, tile2: tileManager.Tile, tile2RowAndColumn):
        self.owners = []
        self.tile1 = tile1
        self.tile1Pos = notationObject.RowAndColumn(tile1RowAndColumn.row, tile1RowAndColumn. col)
        self.tile2 = tile2
        self.tile2Pos = notationObject.RowAndColumn(tile2RowAndColumn.row, tile2RowAndColumn.col)
        if tile1.isBasicFlower():
            self.addOwner(self.tile1.ownerCode)
        elif tile2.isBasicFlower():
            self.addOwner(self.tile2.ownerCode)

    def addOwner(self, ownerCode) -> None:
        if ownerCode not in self.owners:
            self.owners.append(ownerCode)

    def hasOwner(self, ownerCode):
        return ownerCode in self.owners

    def equals(self, otherHarmony: Harmony) -> bool:
        if self.tile1 == otherHarmony.tile1 or self.tile1 == otherHarmony.tile2:
            if self.tile2 == otherHarmony.tile1 or self.tile2 == otherHarmony.tile2:
                return True

        return False

    def notAnyOfThese(self, harmonies) -> bool:
        for harmony in harmonies:
            if self.equals(harmony):
                return False

        return True

    def containsTile(self, tile: tileManager.Tile) -> bool:
        return self.tile1 == tile or self.tile2 == tile

    def getTileThatIsNotThisOne(self, tile):
        if self.tile1 == tile:
            return self.tile2
        elif self.tile2 == tile:
            return self.tile1
        else:
            print("BOTH TILES ARE NOT THAT ONE!")

    def containsTilePos(self, pos):
        return self.tile1Pos.samesies(pos) or self.tile2Pos.samesies(pos)

    def getPosThatIsNotThisOne(self, pos):
        if self.tile1Pos.samesies(pos):
            return self.tile2Pos
        elif self.tile2Pos.samesies(pos):
            return self.tile1Pos
        else:
            print("BOTH TILES ARE NOT THAT ONE!")

    def getString(self):
        return str(self.owners) + " (" + self.tile1Pos.notationPointString + ")-(" + self.tile2Pos.notationPointString + ")"

    def getDirectionForTile(self, tile: tileManager.Tile):
        if not self.containsTile(tile):
            return

        thisPos = self.tile1Pos
        otherPos = self.tile2Pos
        if self.tile2.id == tile.id:
            thisPos = self.tile2Pos
            otherPos = self.tile1Pos

        if thisPos.row == otherPos.row:
            if thisPos.col < otherPos.col:
                return "East"
            else:
                return "West"
        elif thisPos.col == otherPos.col:
            if thisPos.row > otherPos.row:
                return "North"
            else:
                return "South"

    def crossesMidLine(self):
        rowHigh = self.tile1Pos.row
        rowLow = self.tile2Pos.row
        if self.tile1Pos.row < self.tile2Pos.row:
            rowHigh = self.tile2Pos.row
            rowLow = self.tile1Pos.row

        if rowHigh != rowLow:
            return rowHigh > 8 > rowLow and self.tile1Pos.col != 8

        colHigh = self.tile1Pos.col
        colLow = self.tile2Pos.col
        if self.tile1Pos.col < self.tile2Pos.col:
            colHigh = self.tile2Pos.col
            colLow = self.tile1Pos.col

        if colHigh != colLow:
            return colHigh > 8 > colLow and self.tile1Pos.row != 8

    def crossesCenter(self):
        rowHigh = self.tile1Pos.row
        rowLow = self.tile2Pos.row
        if self.tile1Pos.row < self.tile2Pos.row:
            rowHigh = self.tile2Pos.row
            rowLow = self.tile1Pos.row

        if rowHigh != rowLow:
            return rowHigh > 8 > rowLow

        colHigh = self.tile1Pos.col
        colLow = self.tile2Pos.col
        if self.tile1Pos.col < self.tile2Pos.col:
            colHigh = self.tile2Pos.col
            colLow = self.tile1Pos.col

        if colHigh != colLow:
            return colHigh > 8 > colLow


class HarmonyManager:
    def __init__(self):
        self.harmonies = []

    def printHarmonies(self):
        for harmony in self.harmonies:
            harmony.getString()

    def getHarmoniesWithThisTile(self, tile: tileManager.Tile):
        results = []
        for harmony in self.harmonies:
            if harmony.containsTile(tile):
                results.append(harmony)

        return results

    def addHarmony(self, harmony: Harmony):
        harmonyIndexesToRemove = []
        exists = False
        for j in range(len(self.harmonies)):
            if harmony.equals(self.harmonies[j]):
                exists = True

        for i in harmonyIndexesToRemove:
            del self.harmonies[i]

        if not exists:
            self.harmonies.append(harmony)

    def addHarmonies(self, harmoniesToAdd):
        for harmony in harmoniesToAdd:
            self.addHarmony(harmony)

    def clearList(self):
        self.harmonies = []

    def numHarmoniesForPlayer(self, player):
        count = 0
        for harmony in self.harmonies:
            if harmony.hasOwner(player):
                count += 1

        return count

    def getPlayerWithMostHarmonies(self):
        hostCount = self.numHarmoniesForPlayer(gameManager.HOST)
        guestCount = self.numHarmoniesForPlayer(gameManager.GUEST)

        if guestCount > hostCount:
            return gameManager.GUEST
        elif hostCount > guestCount:
            return gameManager.HOST

    def getPlayerWithMostHarmoniesCrossingMidlines(self):
        hostCount = self.getNumCrossingMidlinesForPlayer(gameManager.HOST)
        guestCount = self.getNumCrossingMidlinesForPlayer(gameManager.GUEST)

        if guestCount > hostCount:
            return gameManager.GUEST
        elif hostCount > guestCount:
            return gameManager.HOST

    def getNumCrossingMidlinesForPlayer(self, player):
        count = 0
        for harmony in self.harmonies:
            if harmony.hasOwner(player) and harmony.crossesMidLine():
                count += 1

        return count

    def getNumCrossingCenterForPlayer(self, player):
        count = 0
        for harmony in self.harmonies:
            if harmony.hasOwner(player) and harmony.crossesCenter():
                count += 1

        return count

    def ringLengthForPlayer(self, player):
        rings = self.getHarmonyChains()
        longest = 0

        for ring in rings:
            h = ring.pop()
            if h.hasOwner(player):
                if ring.length > longest:
                    longest = ring.length

        return longest

    def chainLengthCrossingMiddleForPlayer(self, player):
        chains = []
        for i in range(len(self.harmonies)):
            if not self.harmonies[i].hasOwner(player) or not self.harmonies[i].crossesMidLine():
                continue

            chain = [self.harmonies[i]]
            chainTiles = [self.harmonies[i].tile1, self.harmonies[i].tile2]
            j = 0
            while j < len(self.harmonies):
                if self.harmonies[j].hasOwner(player) and self.harmonies[j].crossesMidLine() and self.harmonies[j].tile1 in chainTiles and self.harmonies[j].tile2 not in chainTiles:
                    chainTiles.append(self.harmonies[j].tile2)
                    chain.append(self.harmonies[j])
                    j = 0

                if self.harmonies[j].hasOwner(player) and self.harmonies[j].crossesMidLine() and self.harmonies[j].tile2 in chainTiles and self.harmonies[j].tile1 not in chainTiles:
                    chainTiles.append(self.harmonies[j].tile1)
                    chain.append(self.harmonies[j])
                    j = 0

                j += 1
            chains.append(chain)

        longest = 0
        for chain in chains:
            count = 0
            for h in chain:
                if h.hasOwner(player):
                    count += 1

            if count > longest:
                longest = len(chain)

        return longest

    def chainQuadrantNumberForPlayer(self, player):
        chains = []
        chainsTiles = []
        for i in range(len(self.harmonies)):
            if not self.harmonies[i].hasOwner(player):
                continue

            chain = [self.harmonies[i]]
            chainTiles = [self.harmonies[i].tile1, self.harmonies[i].tile2]
            j = 0
            while j < len(self.harmonies):
                if self.harmonies[j].hasOwner(player) and self.harmonies[j].tile1 in chainTiles and self.harmonies[j].tile2 not in chainTiles:
                    chainTiles.append(self.harmonies[j].tile2)
                    chain.append(self.harmonies[j])
                    j = 0

                if self.harmonies[j].hasOwner(player) and self.harmonies[j].tile2 in chainTiles and self.harmonies[j].tile1 not in chainTiles:
                    chainTiles.append(self.harmonies[j].tile1)
                    chain.append(self.harmonies[j])
                    j = 0

                j += 1
            chains.append(chain)
            chainsTiles.append(chainsTiles)


        longest = 0
        for i, chain in enumerate(chains):
            isInFirstQuadrant = False
            isInSecondQuadrant = False
            isInThirdQuadrant = False
            isInFourthQuadrant = False
            for harmony in chain:
                if (harmony.tile1Pos.row < 8 and harmony.tile1Pos.col < 8) or (harmony.tile2Pos.row < 8 and harmony.tile2Pos.col < 8):
                    isInFirstQuadrant = True

                if (harmony.tile1Pos.row < 8 and harmony.tile1Pos.col > 8) or (harmony.tile2Pos.row < 8 and harmony.tile2Pos.col > 8):
                    isInSecondQuadrant = True

                if (harmony.tile1Pos.row > 8 and harmony.tile1Pos.col < 8) or (harmony.tile2Pos.row > 8 and harmony.tile2Pos.col < 8):
                    isInThirdQuadrant = True

                if (harmony.tile1Pos.row > 8 and harmony.tile1Pos.col > 8) or (harmony.tile2Pos.row > 8 and harmony.tile2Pos.col > 8):
                    isInFourthQuadrant = True

            count = int(isInFirstQuadrant) + int(isInSecondQuadrant) + int(isInThirdQuadrant) + int(isInFourthQuadrant)

            if count > longest:
                longest = count

        return longest

    def getPlayerWithLongestChain(self):
        hostCount = self.ringLengthForPlayer(gameManager.HOST)
        guestCount = self.ringLengthForPlayer(gameManager.GUEST)

        if guestCount > hostCount:
            return gameManager.GUEST
        elif hostCount > guestCount:
            return gameManager.HOST

    def hasNewHarmony(self, player, oldHarmonies):
        newHarmonies = []
        for harmony in self.harmonies:
            if harmony.hasOwner(player):
                exists = False
                for oldHarmony in oldHarmonies:
                    if harmony.equals(oldHarmony) and oldHarmony.hasOwner(player):
                        exists = True

                if not exists:
                    newHarmonies.append(harmony)

        return len(newHarmonies) > 0

    def getHarmonyChains(self):
        rings = []
        for harmony in self.harmonies:
            chain = [harmony]
            startTile = harmony.tile2
            targetTile = harmony.tile1

            foundRings = self.lookForRings(startTile, targetTile, chain)

            if len(foundRings) > 0:
                for ringFound in foundRings:
                    ringExists = False
                    for ring in rings:
                        if self.ringsMatch(ring, ringFound):
                            ringExists = True

                    if not ringExists:
                        rings.append(ringFound)

        return rings


    def harmonyRingExists(self):
        rings = self.getHarmonyChains()
        verifiedHarmonyRingOwners = []
        for ring in rings:
            playerCode = self.verifyHarmonyRing(ring)
            if playerCode is not None:
                verifiedHarmonyRingOwners.append(playerCode)

        return verifiedHarmonyRingOwners

    def verifyHarmonyRing(self, ring):
        shapePoints = []
        allHaveHost = True
        allHaveGuest = True
        for i in range(len(ring)):
            if not ring[i].hasOwner(gameManager.HOST):
                allHaveHost = False
            if not ring[i].hasOwner(gameManager.GUEST):
                allHaveGuest = False

        playerNames = ""
        if allHaveHost and allHaveGuest:
            playerNames = "Host and Guest"
        elif allHaveHost:
            playerNames = gameManager.HOST
        elif allHaveGuest:
            playerNames = gameManager.GUEST

        h = ring[0]

        shapePoints.append(notationObject.NotationPoint(h.tile1Pos.notationPointString).toArr())
        shapePoints.append(notationObject.NotationPoint(h.tile2Pos.notationPointString).toArr())

        lastTilePos = h.tile2Pos

        count = 0
        connected = False
        harmoniesFound = [h]
        while not connected and count < 400:
            for i in range(len(ring)):
                h = ring[i]
                if h.containsTilePos(lastTilePos) and h not in harmoniesFound:
                    lastTilePos = h.getPosThatIsNotThisOne(lastTilePos)
                    harmoniesFound.append(h)
                    np = notationObject.NotationPoint(lastTilePos.notationPointString)
                    if not np.samesies(
                            notationObject.NotationPoint(str(shapePoints[0][0]) + "," + str(shapePoints[0][1]))):
                        shapePoints.append(np.toArr())
                    if len(harmoniesFound) == len(ring):
                        connected = True

            count += 1

        if count > 390:
            #print("THERE WAS A PROBLEM CONNECTING THE DOTS")
            return False

        if self.isCenterInsideShape(shapePoints):
            return playerNames

    def isPointInsideShape(self, notationPoint, shapePoints):
        x = notationPoint.x
        y = notationPoint.y

        inside = False
        j = len(shapePoints) - 1
        for i in range(len(shapePoints)):
            xi = shapePoints[i][0]
            yi = shapePoints[i][1]
            xj = shapePoints[j][0]
            yj = shapePoints[j][1]

            if xi == x and xj == x and xi * xj:
                return False

            if yj - yi != 0:
                intersect = ((yi > y) != (yj > y)) and (x < (xj - xi) * (y - yi) / (yj - yi) + xi)
                if intersect:
                    inside = not inside

            j = i

        return inside

    def isCenterInsideShape(self, vs):
        x = 0
        y = 0

        wn = 0
        j = len(vs) - 1
        for i in range(len(vs)):
            xi = float(vs[i][0])
            yi = float(vs[i][1])
            xj = float(vs[j][0])
            yj = float(vs[j][1])

            if (xi == 0 and xj == 0 and yi * yj < 0) or (yi == 0 and yj == 0 and xi * xj < 0):
                # print("crosses center, cannot count")
                return False

            if (xi == 0 and yi == 0) or (xj == 0 and yj == 0):
                # print("On center point, cannot count")
                return False

            if yj <= y:
                if yi > y:
                    if self.isLeft([xj, yj], [xi, yi], [x, y]) > 0:
                        wn += 1
            else:
                if yi <= y:
                    if self.isLeft([xj, yj], [xi, yi], [x, y]) < 0:
                        wn -= 1

        return wn != 0

    def isLeft(self, P0, P1, P2):
        res = ((P1[0] - P0[0]) - (P2[1] - P0[1])) - ((P2[0] - P0[0]) * (P1[1] - P0[1]))
        return res

    def ringsMatch(self, ring1, ring2):
        if len(ring1) != len(ring2):
            return False

        h1Matches = False
        for h1 in ring1:
            h1Matches = False
            for h2 in ring2:
                if h1.equals(h2):
                    h1Matches = True

            if not h1Matches:
                return False

        return h1Matches

    def lookForRings(self, t1, tx, originalChain):
        rings = []
        keepLookingAtTheseHarmonies = []
        for i in range(len(self.harmonies)):
            currentChain = originalChain.copy()
            hx: Harmony = self.harmonies[i]
            if hx.containsTile(t1) and hx.notAnyOfThese(currentChain):
                currentChain.append(hx)
                if hx.containsTile(tx):
                    rings.append(currentChain)
                else:
                    keepLookingAtTheseHarmonies.append(hx)

        for i in range(len(self.harmonies)):
            currentChain = originalChain.copy()
            hx = self.harmonies[i]
            if hx in keepLookingAtTheseHarmonies:
                currentChain.append(hx)
                newStartTile = hx.getTileThatIsNotThisOne(t1)
                rings = rings + self.lookForRings(newStartTile, tx, currentChain)

        return rings

    def lookForRing(self, t1, tx, chain):
        for i in range(len(self.harmonies)):
            hx: Harmony = self.harmonies[i]
            if hx.containsTile(t1) and hx.notAnyOfThese(chain):
                chain.append(hx)
                if hx.containsTile(tx):
                    return [True, chain]
                else:
                    newStartTile = hx.getTileThatIsNotThisOne(t1)
                    return self.lookForRing(newStartTile, tx, chain)

        return [False]
