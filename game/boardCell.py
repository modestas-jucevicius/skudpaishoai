from __future__ import annotations

import game.tileManager as tileManager
import game.gameManager as gameManager
import typing
NON_PLAYABLE = "NonPlayable"
NEUTRAL = "Neutral"
GATE = "Gate"
RED = 'R'
WHITE = 'W'
POSSIBLE_MOVE = "PossibleMove"


class BoardCell:
    def __init__(self):
        self.types = []
        self.row: int = -1
        self.col: int = -1
        self.tile: typing.Optional[tileManager.Tile] = None
        self.betweenHarmony: bool = False
        self.betweenHarmonyGuest: bool = False
        self.betweenHarmonyHost: bool = False

    def addType(self, cellType) -> None:
        self.types.append(cellType)

    def removeType(self, cellType) -> None:
        if cellType in self.types:
            self.types.remove(cellType)

    def putTile(self, tile: tileManager.Tile):
        self.tile = tile

    def hasTile(self) -> bool:
        if self.tile is not None:
            return True
        else:
            return False

    def isType(self, type) -> bool:
        return type in self.types

    def isOpenGate(self) -> bool:
        return self.tile is None and GATE in self.types

    def removeTile(self) -> tileManager.Tile:
        removedTile = self.tile
        self.tile = None
        return removedTile

    def canHoldTile(self, tile: tileManager.Tile, ignoreTileCheck: bool) -> bool:
        if NON_PLAYABLE in self.types:
            return False

        if not ignoreTileCheck and BoardCell.hasTile(self):
            return False

        if tileManager.Tile.isBasicFlower(tile):
            if not (NEUTRAL in self.types or tileManager.Tile.getBasicColorCode(tile) in self.types):
                return False

            if GATE in self.types:
                return False

            return True

        if tileManager.Tile.isSpecialFlower(tile):
            return True

        if tileManager.Tile.isAccentTile(tile):
            return True

        return False

    def betweenPlayerHarmony(self, player: str) -> bool:
        if player == gameManager.GUEST:
            return self.betweenHarmonyGuest
        if player == gameManager.HOST:
            return self.betweenHarmonyHost
        return False

    def getCopy(self) -> BoardCell:
        copy = BoardCell()
        copy.setTypes(self.types.copy())
        copy.setRow(self.row)
        copy.setCol(self.col)
        if self.hasTile():
            copy.putTile(tileManager.Tile.getCopy(self.tile))

        return copy

    def setBetweenHarmonyGuest(self, value: bool) -> None:
        self.betweenHarmonyGuest = value

    def setBetweenHarmonyHost(self, value: bool) -> None:
        self.betweenHarmonyHost = value

    def getBetweenHarmonyGuest(self) -> bool:
        return self.betweenHarmonyGuest

    def getBetweenHarmonyHost(self) -> bool:
        return self.betweenHarmonyHost

    def getTypes(self) -> list:
        return self.types

    def setTypes(self, types: list) -> None:
        self.types = types

    def getRow(self) -> int:
        return self.row

    def setRow(self, row) -> None:
        self.row = row

    def getCol(self) -> int:
        return self.col

    def setCol(self, col) -> None:
        self.col = col

    def drainTile(self) -> None:
        if self.hasTile():
            self.tile.drain()

    def restoreTile(self) -> None:
        if self.hasTile():
            self.tile.restore()


def neutral() -> BoardCell:
    cell = BoardCell()
    cell.addType(NEUTRAL)
    return cell


def gate() -> BoardCell:
    cell = BoardCell()
    cell.addType(GATE)
    return cell


def red() -> BoardCell:
    cell = BoardCell()
    cell.addType(RED)
    return cell


def white() -> BoardCell:
    cell = BoardCell()
    cell.addType(WHITE)
    return cell


def redWhite() -> BoardCell:
    cell = BoardCell()
    cell.addType(RED)
    cell.addType(WHITE)
    return cell


def redWhiteNeutral() -> BoardCell:
    cell = BoardCell()
    cell.addType(RED)
    cell.addType(WHITE)
    cell.addType(NEUTRAL)
    return cell


def redNeutral() -> BoardCell:
    cell = BoardCell()
    cell.addType(RED)
    cell.addType(NEUTRAL)
    return cell


def whiteNeutral() -> BoardCell:
    cell = BoardCell()
    cell.addType(WHITE)
    cell.addType(NEUTRAL)
    return cell


def nonPlayable() -> BoardCell:
    cell = BoardCell()
    cell.addType(NON_PLAYABLE)
    return cell

