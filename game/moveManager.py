import itertools
import random
from joblib import Parallel, delayed
import itertools

import game.boardCell as boardCell
#import boardManager
import game.gameManager as gameManager
import game.notationObject as notationObject
import game.tileManager as tileManager

skudAccentTiles = ['R', 'W', 'K', 'B', 'R', 'W', 'K', 'B']


class MoveManager:
    def __init__(self):
        self.moveNum = 0

    def getPossibleMoves(self, board, player):
        moves = []
        if self.moveNum == 0:
            moves += self.getAccentSelectionMoves(player)
        else:
            moves += self.getArrangeMoves(board, player)
            moves += self.getPlantMoves(board, player)

        return moves

    def getPlantMoves(self, board, player):
        if not isOpenGate(board):
            return []

        moves = []
        tilePile = getTilePile(board, player)
        tilesPlanted = []
        for tile in tilePile:
            if tile.isBasicFlower() and tile.code not in tilesPlanted:
                tilesPlanted.append(tile.code)
                board.setOpenGatePossibleMoves(player, tile)
                endPoints = getPossibleMovePoints(board)
                board.removePossibleMoveCells()
                for endPoint in endPoints:
                    notationBuilder = notationObject.NotationBuilder()
                    notationBuilder.moveType = notationObject.PLANTING
                    notationBuilder.plantedFlowerType = tile.code
                    notationBuilder.status = notationObject.WAITING_FOR_ENDPOINT
                    notationBuilder.endPoint = notationObject.NotationPoint(getNotation(endPoint))
                    move = notationBuilder.getNotationMove(self.moveNum, player)
                    moves.append(move)

        return moves

    def getBonusPlantMoves(self, board, player):
        if not isOpenGate(board):
            return []

        moves = []
        tilePile = getTilePile(board, player)
        tilesPlanted = []
        for tile in tilePile:
            if board.playerHasNoGrowingFlowers(player):
                if (tile.isBasicFlower() or tile.isSpecialFlower()) and tile.code not in tilesPlanted:
                    tilesPlanted.append(tile.code)
                    board.setOpenGatePossibleMoves(player, tile)
                    endPoints = getPossibleMovePoints(board)
                    board.removePossibleMoveCells()
                    for endPoint in endPoints:
                        notationBuilder = notationObject.NotationBuilder()
                        notationBuilder.moveType = notationObject.PLANTING
                        notationBuilder.plantedFlowerType = tile.code
                        notationBuilder.status = notationObject.WAITING_FOR_ENDPOINT
                        notationBuilder.endPoint = notationObject.NotationPoint(getNotation(endPoint))
                        move = notationBuilder.getNotationMove(self.moveNum, player)
                        moves.append(move)
            else:
                if tile.isSpecialFlower() and tile.code not in tilesPlanted:
                    tilesPlanted.append(tile.code)
                    board.setOpenGatePossibleMoves(player, tile)
                    endPoints = getPossibleMovePoints(board)
                    board.removePossibleMoveCells()
                    for endPoint in endPoints:
                        notationBuilder = notationObject.NotationBuilder()
                        notationBuilder.moveType = notationObject.PLANTING
                        notationBuilder.plantedFlowerType = tile.code
                        notationBuilder.status = notationObject.WAITING_FOR_ENDPOINT
                        notationBuilder.endPoint = notationObject.NotationPoint(getNotation(endPoint))
                        move = notationBuilder.getNotationMove(self.moveNum, player)
                        moves.append(move)


        return moves

    def getBonusAccentMoves(self, board, player):
        moves = []
        remainingAccentTiles = board.tileManager.remainingAccentTilesHost
        if player == gameManager.GUEST:
            remainingAccentTiles = board.tileManager.remainingAccentTilesGuest

        if remainingAccentTiles == 0:
            return moves

        tilePile = getTilePile(board, player)
        checkedAccents = []
        for tile in tilePile:
            if tile.isAccentTile() and tile.code not in checkedAccents:
                checkedAccents.append(tile.code)
                board.revealPossiblePlacementCells(tile)
                endPoints = getPossibleMovePoints(board)
                board.removePossibleMoveCells()
                moves += list(itertools.chain.from_iterable(Parallel(n_jobs=-1)(delayed(self.getBonusAccentMovesFromEndPoint)(tile.getCopy(), endPoint.getCopy(), board.getCopy(), player) for endPoint in endPoints)))

        return moves

    def getBonusAccentMovesFromEndPoint(self, tile, endPoint, board, player):
        moves = []
        notationBuilder = notationObject.NotationBuilder()
        notationBuilder.moveType = notationObject.PLANTING
        notationBuilder.plantedFlowerType = tile.code
        notationBuilder.status = notationObject.WAITING_FOR_ENDPOINT
        notationBuilder.endPoint = notationObject.NotationPoint(getNotation(endPoint))
        if tile.isTileType(tileManager.BOAT):
            board.revealBoatBonusCells(endPoint)
            boatBonusPoints = getPossibleMovePoints(board)
            board.removePossibleMoveCells()
            for boatBonusPoint in boatBonusPoints:
                notationBuilder.boatBonusPoint = notationObject.NotationPoint(getNotation(boatBonusPoint))
                move = notationBuilder.getNotationMove(self.moveNum, player)
                moves.append(move)
        else:
            move = notationBuilder.getNotationMove(self.moveNum, player)
            moves.append(move)

        return moves

    def getArrangeMoves(self, board, player):
        startPoints = getStartPoints(board, player)
        moves = list(itertools.chain.from_iterable(Parallel(n_jobs=-1)(delayed(self.getArrangeMovesFromStartPoint)(startPoint.getCopy(), board.getCopy(), player) for startPoint in startPoints)))

        return moves

    def getArrangeMovesFromStartPoint(self, startPoint, board, player):
        moves = []
        if not startPoint.hasTile():
            return moves

        endPoints = board.getPossibleMoveCells(startPoint)
        moves = list(itertools.chain.from_iterable(Parallel(n_jobs=-1)(
            delayed(self.getArrangeMovesFromEndPoint)(startPoint.getCopy(), endPoint.getCopy(), board.getCopy(), player) for endPoint in endPoints)))

        return moves

    def getArrangeMovesFromEndPoint(self, startPoint, endPoint, board, player):
        moves = []
        notationBuilder = notationObject.NotationBuilder()
        notationBuilder.status = notationObject.WAITING_FOR_ENDPOINT
        notationBuilder.moveType = notationObject.ARRANGING
        notationBuilder.startPoint = notationObject.NotationPoint(getNotation(startPoint))
        notationBuilder.endPoint = notationObject.NotationPoint(getNotation(endPoint))
        move = notationBuilder.getNotationMove(self.moveNum, player)
        result = board.moveTile(player,
                                    notationObject.RowAndColumn(startPoint.row, startPoint.col).getNotationPoint(),
                                    notationObject.RowAndColumn(endPoint.row, endPoint.col).getNotationPoint())
        if len(result) > 1:
            moves.append(move)
        # else:
        #     print("Debug")
        if result[0] is True:
            board.removePossibleMoveCells()
            bonusMoves = self.getBonusPlantMoves(board, player)
            for bonusMove in bonusMoves:
                notationBuilder.bonusTileCode = bonusMove.plantedFlowerType
                notationBuilder.bonusEndPoint = bonusMove.endPoint
                newMove = notationBuilder.getNotationMove(self.moveNum, player)
                moves.append(newMove)

            # bonusMoves = self.getBonusAccentMoves(board, player)
            # moves += Parallel(n_jobs=-1)(
            #     delayed(self.getArrangeMovesFromBonusAccentMoves)(bonusMove, notationBuilder.getCopy(), player) for bonusMove in
            #     bonusMoves)

        return moves

    def getArrangeMovesFromBonusAccentMoves(self, bonusMove, notationBuilder, player):
        notationBuilder.bonusTileCode = bonusMove.plantedFlowerType
        notationBuilder.bonusEndPoint = bonusMove.endPoint
        notationBuilder.boatBonusPoint = bonusMove.boatBonusPoint
        newMove = notationBuilder.getNotationMove(self.moveNum, player)
        return newMove

    def getAccentSelectionMoves(self, player):
        moves = []
        accentSets = getAvailableAccentSets()
        for accentSet in accentSets:
            move = notationObject.NotationMove("0" + player[0] + "." + str(accentSet))
            moves.append(move)

        return moves

    def getCopy(self):
        newManager = MoveManager()
        newManager.moveNum = self.moveNum
        return newManager


def runNotationMove(move: notationObject.NotationMove, board):
    errorFound = False
    bonusAllowed = False
    if move.moveNum == 0 and move.accentTiles is not None:
        allAccentCodes = skudAccentTiles.copy()
        for tileCode in move.accentTiles:
            del allAccentCodes[allAccentCodes.index(tileCode)]

        for tileCode in allAccentCodes:
            board.tileManager.grabTile(move.player, tileCode)

    if move.moveType == notationObject.PLANTING:
        if not board.cellIsOpenGate(move.endPoint):
            errorFound = True
            print("Point is not open Gate")
            return False

        tile = board.tileManager.grabTile(move.player, move.plantedFlowerType)
        board.placeTile(tile, move.endPoint, move.boatBonusPoint)
    elif move.moveType == notationObject.ARRANGING:
        moveResults = board.moveTile(move.player, move.startPoint, move.endPoint)
        bonusAllowed = moveResults[0]
        move.capturedTile = moveResults[2]
        if bonusAllowed and move.hasHarmonyBonus():
            tile = board.tileManager.grabTile(move.player, move.bonusTileCode)
            move.accentTileUsed = tile
            if move.boatBonusPoint is not None:
                board.placeTile(tile, move.bonusEndPoint, move.boatBonusPoint)
            else:
                placeTileResult = board.placeTile(tile, move.bonusEndPoint, move.boatBonusPoint)
                if placeTileResult is not None:
                    move.tileRemovedWithBoat = placeTileResult
        elif moveResults == False and move.hasHarmonyBonus():
            print("Bonus not allowed")
            errorFound = True

    if len(board.winners) == 0:
        playerOutOfTiles = board.tileManager.aPlayerIsOutOfBasicFlowerTiles()
        if playerOutOfTiles is not None:
            playerWithMostHarmonies = board.harmonyManager.getPlayerWithMostHarmoniesCrossingMidlines()
            if playerWithMostHarmonies is not None:
                board.winners.append(playerWithMostHarmonies)
            else:
                board.winners += [gameManager.GUEST, gameManager.HOST]

    board.getUniqueGameStateId()
    if move.moveNum == 0 and move.player == "G":
        board.moveManager.moveNum += 1

    if move.moveNum > 0 and move.player == "H":
        board.moveManager.moveNum += 1

    if not (move.moveNum == 0 and move.player == "G"):
        if move.player == "G":
            board.playerCode = "H"
        else:
            board.playerCode = "G"

    return bonusAllowed


def getStartPoints(board, player):
    points = []
    for row in board.cells:
        for cell in row:
            if cell.hasTile() and cell.tile.ownerCode == player and not cell.tile.isAccentTile() and not (cell.tile.drained or cell.tile.trapped):
                points.append(cell)

    return points


def getNotation(boardPoint):
    return notationObject.RowAndColumn(boardPoint.row, boardPoint.col).notationPointString


def getAvailableAccentSets():
    accentSets = itertools.combinations(skudAccentTiles, 4)
    return list(map(",".join, accentSets))


def getPossibleMovePoints(board):
    points = []
    for row in board.cells:
        for cell in row:
            if cell.isType(boardCell.POSSIBLE_MOVE):
                points.append(cell)

    return points


def getTilePile(board, player):
    tilePile = board.tileManager.hostTiles
    if player == gameManager.GUEST:
        tilePile = board.tileManager.guestTiles

    return tilePile


def isOpenGate(board):
    for row in board.cells:
        for cell in row:
            if cell.isOpenGate():
                return True


# for i in range(500):
#     fullGameText = ""
#     #moveHistory = []
#     testBoard = boardManager.Board("H")
#     moveManager = MoveManager()
#     #print(moveManager.getAccentSelectionMoves("G")[0].fullMoveText)
#     #print(moveManager.getAccentSelectionMoves("H")[0].fullMoveText)
#     #moveHistory.append(moveManager.getAccentSelectionMoves("H")[0])
#     #moveHistory.append(moveManager.getAccentSelectionMoves("G")[0])
#
#     while len(testBoard.winners) == 0:
#         possibleMoves = moveManager.getPossibleMoves(testBoard, testBoard.playerCode)
#         randomMove = random.randint(0, len(possibleMoves) - 0001)
#         #print(possibleMoves[randomMove].fullMoveText)
#         fullGameText += possibleMoves[randomMove].fullMoveText
#         fullGameText += ';'
#         runNotationMove(possibleMoves[randomMove], testBoard)
#         #moveHistory.append(possibleMoves[randomMove])
#         possibleMoves = moveManager.getPossibleMoves(testBoard, testBoard.playerCode)
#         randomMove = random.randint(0, len(possibleMoves) - 0001)
#         #print(possibleMoves[randomMove].fullMoveText)
#         fullGameText += possibleMoves[randomMove].fullMoveText
#         fullGameText += ';'
#         runNotationMove(possibleMoves[randomMove], testBoard)
#         moveManager.moveNum += 0001
#         #moveHistory.append(possibleMoves[randomMove])
#         print(fullGameText)
#
#     print(fullGameText)


