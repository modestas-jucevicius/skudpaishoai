import torch.nn as nn
import torch

import config

filters = 256
kernelSize = (3, 3)
device = None
# print("Using {} device".format(device))


class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.nn_layers = nn.ModuleList()
        self.loss_fn = nn.CrossEntropyLoss()
        self.nn_layers.append(nn.Linear(1156, config.LAYER_SIZE))
        self.nn_layers.append(nn.ReLU())
        for i in range(config.HIDDEN_LAYERS):
            self.nn_layers.append(nn.Linear(config.LAYER_SIZE, config.LAYER_SIZE))
            self.nn_layers.append(nn.ReLU())
        self.nn_layers.append(nn.Linear(config.LAYER_SIZE, 3))
        self.softMax = nn.Softmax(dim=1)
        self.optimizer = torch.optim.SGD(self.parameters(), lr=config.LEARNING_RATE, momentum=config.MOMENTUM)

    def forward(self, x):
        for l in self.nn_layers:
            x = l(x)

        return self.softMax(x)


def train(states, targets, model, loss_fn, optimizer):

    state = torch.as_tensor(states, dtype=torch.float32, device=torch.device(device))
    value = torch.as_tensor(targets, dtype=torch.long, device=torch.device(device))

    # Compute prediction error
    pred = model(state)
    loss = loss_fn(pred, value + 1)

    # Backpropagation
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    return loss.item()


def test(states, targets, model, printResult=False):
    size = len(states)
    model.eval()
    test_loss, correct = 0, 0
    with torch.no_grad():
        state = torch.as_tensor(states, dtype=torch.float32, device=torch.device(device))
        value = torch.as_tensor(targets, dtype=torch.long, device=torch.device(device))
        pred = model(state)
        pred_value = torch.argmax(pred, dim=1)
        test_loss += model.loss_fn(pred, value + 1).item()
        correct += (pred_value == value + 1).sum().item()

    if printResult:
        test_loss /= size
        correct /= size
        print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")

    return test_loss


