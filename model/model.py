import torch.nn as nn
import torch

import config

filters = 256
kernelSize = (3, 3)
device = None
# print("Using {} device".format(device))


class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()
        self.flatten = nn.Flatten()
        self.conv = nn.Conv2d(5, filters, kernelSize, stride=1, padding=1)
        self.conv2 = nn.Conv2d(filters, filters, kernelSize, stride=1, padding=1)
        self.batchNorm = nn.BatchNorm2d(filters)
        self.leakyRelu = nn.LeakyReLU()
        self.loss_fn = nn.MSELoss()
        self.optimizer = torch.optim.SGD(self.parameters(), lr=config.LEARNING_RATE)
        self.valueConv = nn.LazyConv2d(1, (1, 1), stride=1)
        self.valueLinear = nn.LazyLinear(1)
        self.valueNorm = nn.BatchNorm2d(1)



    def forward(self, x):
        x = self.conv(x)
        x = self.batchNorm(x)
        x = self.leakyRelu(x)

        for i in range(config.HIDDEN_LAYERS):
            old_x = x
            x = self.conv2(x)
            x = self.batchNorm(x)
            x = self.leakyRelu(x)
            x = self.conv2(x)
            x = self.batchNorm(x)
            x = old_x + x
            x = self.leakyRelu(x)

        value = self.valueConv(x)
        value = self.valueNorm(value)
        value = self.leakyRelu(value)
        value = self.leakyRelu(value)
        value = self.valueLinear(value.flatten(1, 2))
        value = self.valueLinear(value.flatten(-2))

        return value.flatten()


def train(states, targets, model, loss_fn, optimizer):
    model.train()
    state = torch.as_tensor(states, dtype=torch.float32, device=torch.device(device))
    value = torch.as_tensor(targets, dtype=torch.float32, device=torch.device(device))

    # Compute prediction error
    pred = model(state)
    loss = loss_fn(pred, value)

    # Backpropagation
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()

    return loss.item()


def test(dataloader, model):
    size = len(dataloader.dataset)
    model.eval()
    test_loss, correct = 0, 0
    with torch.no_grad():
        for X, y in dataloader:
            X, y = X.to(device), y.to(device)
            pred = model(X)
            test_loss += model.loss_fn(pred, y).item()
            correct += (pred.argmax(1) == y)
    test_loss /= size
    correct /= size
    print(f"Test Error: \n Accuracy: {(100*correct):>0.1f}%, Avg loss: {test_loss:>8f} \n")


